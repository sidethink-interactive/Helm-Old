#pragma once

#include "ast_node_list.hpp"
#include "lexer.hpp"
#include "util.hpp"

struct AstFile {
	fs::path path;
	std::vector<Token> tokens;
	std::vector<AstNode*> nodes;
};

AstFile parse_tokens(fs::path path, std::vector<Token> tokens);




#ifdef PARSER_INTERNAL



force_inline_all bool IsLiteral(TokenKind kind);
force_inline_all bool IsArithmetic(TokenKind kind);
force_inline_all bool IsAssignment(TokenKind kind);
force_inline_all bool IsComparison(TokenKind kind);

// NOTE(Brendan): Notice that these functions do not take index by reference. This is on purpose.
// Since this is merely returning a boolean about a part of the code, not parsing it, we don't want it moving index around.
force_inline_all bool IsType(std::vector<Token> tokens, u32 index);
force_inline_all bool IsValue(std::vector<Token> tokens, u32 index);

force_inline_all bool CreateNewBlock(Token token);
force_inline_all bool CloseCurrentBlock(Token token);


//std::vector<AstNode*> parse_lhs(AstFile* file);
//AstNode* parse_proc_lit();

#endif



AstNode* parse_if(std::vector<Token> tokens, u32 &index);
AstNode* parse_for(std::vector<Token> tokens, u32 &index);
AstNode* parse_for_in(std::vector<Token> tokens, u32 &index);
AstNode* parse_ellipsis(std::vector<Token> tokens, u32 &index);

std::vector<AstNode*> parse_var_creation(std::vector<AstNode*> floating_identifiers, std::vector<Token> tokens, u32 &index, bool funcStyleParse = false);
AstNode* parse_var_assignment(std::vector<AstNode*> floating_identifiers, std::vector<Token> tokens, u32 &index);

std::vector<AstNode*> parse_args(std::vector<Token> tokens, u32 &index, bool declaration = false, bool isStruct = false, bool isImplicitArray = false);
AstNode* parse_function_definition(std::vector<Token> tokens, u32 &index);

AstNode* GetType(std::vector<Token> tokens, u32 &index, bool indexReturn = false, bool ignoreImplicit = false);

AstNode* parse_expression(std::vector<Token> tokens, u32 &index, u32 cutoff = 0);