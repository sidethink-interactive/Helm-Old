#pragma once

#if defined(_WIN32) || defined(_WIN64) || defined(__CYGWIN__) || defined(__WIN32__) || defined(__WINDOWS__) || defined(__TOS_WIN__) || defined(__MINGW32__) || defined(__MINGW64__)
#define OS_WINDOWS
#define OS_GARBAGE
#if defined(__GNUC__) || defined(__MINGW32__) || defined(__MINGW64__)
#define STDLIB_GCC
#define OS_MACROS _(OS_WINDOWS) _(OS_GARBAGE) _(STDLIB_GCC)
#else
#define STDLIB_WIN
#define OS_MACROS _(OS_WINDOWS) _(OS_GARBAGE) _(STDLIB_WIN)
#endif
#elif defined(__FreeBSD__) || defined(__NetBSD__) || defined(__OpenBSD__) || defined(__bsdi__) || defined(__DragonFly__) || defined(BSD)
#define OS_BSD
#define OS_NIX
#define OS_NIX_NOT_MACOS
#define OS_POSIX
#define BUILD_GTK
#define STDLIB_GCC
#define OS_MACROS _(OS_BSD) _(OS_NIX) _(OS_NIX_NOT_MACOS) _(OS_POSIX) _(BUILD_GTK) _(STDLIB_GCC)
#elif defined(__APPLE__) || defined(__MACH__)
#define OS_MACOS
#define OS_NIX
#define OS_POSIX
#define STDLIB_CLANG
#define OS_MACROS _(OS_MACOS) _(OS_NIX) _(OS_POSIX) _(STDLIB_CLANG)
#elif defined(__GNU__) || defined(__gnu_hurd__)
#define OS_HURD
#define OS_NIX
#define OS_NIX_NOT_MACOS
#define BUILD_GTK
#define STDLIB_GCC
#define OS_MACROS _(OS_HURD) _(OS_NIX) _(OS_NIX_NOT_MACOS) _(BUILD_GTK) _(STDLIB_GCC)
#elif defined(__gnu_linux__) || defined(__linux__) || defined(linux) || defined(__linux)
#define OS_LINUX
#define OS_NIX
#define OS_NIX_NOT_MACOS
#define BUILD_GTK
#define STDLIB_GCC
#define OS_MACROS _(OS_LINUX) _(OS_NIX) _(OS_NIX_NOT_MACOS) _(BUILD_GTK) _(STDLIB_GCC)
#else
#error Unsupported OS!
#endif


#if __x86_64 || __x86_64__ || __amd64 || __amd64__ || _LP64 || __LP64__ || (defined _WIN64 && !defined _WIN32)
#define OS_64
#else
#define OS_32
#endif