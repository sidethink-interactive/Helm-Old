#pragma once

#include <string>
#include <vector>
#include "token_list.hpp"
#include "util.hpp"

struct TokenPos {
	File* file;
	u32 line, column;

	string get_text_pos() { return "(" + std::to_string(line) + ", " + std::to_string(column) + ")";}
};

struct Token {
	TokenKind kind;
	string text;
	TokenPos pos;
};

#define btwn(a,b,c) ((a)>(b)&&(a)<(c))

#ifdef LEXER_IMPLEMENTATION
	#define bf(n, b) \
		inline bool is_token_ ## n (TokenKind t) { \
			return btwn(t, Token__ ## b ## Begin, Token__ ## b ## End); \
		} \
		inline bool is_token_ ## n (Token t) { \
			return btwn(t.kind, Token__ ## b ## Begin, Token__ ## b ## End); \
		}
	#define bfr(a, b) \
		inline bool is_token_ ## n (TokenKind t) { \
			return b; \
		} \
		inline bool is_token_ ## n (Token _t) { \
			TokenKind t = _t.kind; \
			return b; \
		}
#else
	#define bf(n, b) \
		inline bool is_token_ ## n (TokenKind);\
		inline bool is_token_ ## n (Token);
	#define bfr(a, b) \
		inline bool is_token_ ## n (TokenKind);\
		inline bool is_token_ ## n (Token);
#endif

bf(literal, Literal);
bf(operator, Operator);
bf(comparison, Comparison);
bf(assign, AssignOp);
bf(keyword, Keyword);
bfr(shift, (t==Token_ShiftLeft||t==Token_ShiftRight));

#undef bfr
#undef bf
#undef btwn

#define print_tok(tok) printf("{file: \"%s\", line: %d, col: %d, type: \"%s\", lit: \"%s\"},\n", display_path(tok.pos.file->path).c_str(), tok.pos.line, tok.pos.column, token_names[tok.kind], tok.text.c_str())

std::vector<Token> tokenize_file(fs::path path, string file_contents);
std::vector<Token> tokenize_file(fs::path path);

bool is_whitespace(char test);