#pragma once

#include "types.hpp"
#include <iostream>
#include "libs/ppmagic.h"

string read_file(fs::path path);
void write_file(fs::path filename, string contents);

fs::path make_path(string path);

fs::path get_relative_file(fs::path first_file, string second_file);

extern bool long_paths;

string display_path(string path);
string display_path(fs::path path);

enum CompileFailType {
	NO_FAIL, LEXER_FAIL, PARSER_FAIL, TYPE_CHECK_FAIL, EMIT_FAIL
};

extern CompileFailType fail_type;

struct FullFile {
	void *tokens, *ast, *entities;
};

#define print(...) std::cout << __VA_ARGS__ << std::flush;
#define print_red(...) print("\033[1;31m" << __VA_ARGS__ << "\033[m")
#define print_orn(...) print("\033[1;33m" << __VA_ARGS__ << "\033[m")
#define printf_red(fmt, ...) printf("\033[1;31m" fmt "\033[m" _PP_COMMA_VA(__VA_ARGS__))
#define printf_orn(fmt, ...) printf("\033[1;33m" fmt "\033[m" _PP_COMMA_VA(__VA_ARGS__))

force_inline void error_at(File* file, u32 line, u32 col, string msg, CompileFailType ft);
force_inline void warn_at (File* file, u32 line, u32 col, string msg);