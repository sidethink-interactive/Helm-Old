#pragma once

#include <boost/filesystem.hpp>
#include <string>
#include <vector>

namespace fs = boost::filesystem;

typedef std::string string;

struct File {
	std::vector<string> lines;
	fs::path path;
};

#if defined(_MSC_VER)
	#if _MSC_VER < 1300
	typedef unsigned char     u8;
	typedef   signed char     i8;
	typedef unsigned short   u16;
	typedef   signed short   i16;
	typedef unsigned int     u32;
	typedef   signed int     i32;
	#else
	typedef unsigned __int8   u8;
	typedef   signed __int8   i8;
	typedef unsigned __int16 u16;
	typedef   signed __int16 i16;
	typedef unsigned __int32 u32;
	typedef   signed __int32 i32;
	#endif
	typedef unsigned __int64 u64;
	typedef   signed __int64 i64;
#else
	#include <stdint.h>
	typedef uint8_t   u8;
	typedef  int8_t   i8;
	typedef uint16_t u16;
	typedef  int16_t i16;
	typedef uint32_t u32;
	typedef  int32_t i32;
	typedef uint64_t u64;
	typedef  int64_t i64;
#endif

#if defined(_MSC_VER)
	#if _MSC_VER < 1300
		#define force_inline_all inline
	#else
		#define force_inline_all __forceinline
	#endif
	// MSVC doesn't like force_inline for some reason
	// Where clang is fine, MSVC throws scoping errors.
	// *sigh*
	#define force_inline 
#else
	#define force_inline_all __attribute__ ((__always_inline__))
	// Only clang likes force_inline???
	// Weird. Whatever...
	#ifdef __clang__
		#define force_inline force_inline_all
	#else
		#define force_inline 
	#endif
#endif