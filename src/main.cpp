#include <vector>
#include <stdio.h>
#include "util.hpp"
#include "types.hpp"
#include "platform_name.h"
#include "lexer.hpp"
#include "parser.hpp"
#include "ast_graphviz.hpp"
#ifdef OS_WINDOWS
	#include <windows.h>
#endif

CompileFailType fail_type = NO_FAIL;

#define _usage() printf(\
"%s - The official Helm compiler [ver. 0.1.0]\n"\
"    Usage: %s [command] [arguments]\n"\
"    Commands:\n"\
"        build     | SEE: Standard build options\n"\
"        build_lib | SEE: Standard build options\n"\
"        run       | SEE: Standard build options\n"\
"        ast_graph | filename.helm [-o/--output output file] : Generate graphviz graph of program AST\n"\
"        help\n"\
"    Standard build options:\n"\
"        filename.helm             : The main file to be built.\n"\
"        [-o/--output output file] : Set the name of the output file.\n"\
"                                    Defaults to the input filename, but with .helm replaced\n"\
"                                    with an appropriate extension.\n"\
"        [-D/--define NAME VALUE]  : Add a compile-time constant. SEE: Compile-time constants\n"\
"        [--backend BACKEND]       : Set the backend to be used in compilation.\n"\
"                                    Currently, only LLVM is supported.\n"\
"        [--build-to MODE]         : Set the level of compilation that should occur.\n"\
"                                    bin: Build an executable file. obj: Build an object file.\n"\
"                                    il: Output LLVM IL (intermediary level) assembly. (ONLY ON LLVM BACKEND)\n"\
"    Compile-time constants:\n"\
"        NAME may be any string that is a valid identifier.\n"\
"        VALUE may be a bool (`true`, `false`), an integer (`0`, `-4`),\n"\
"           or a string (default).  Any other types are NOT supported,\n"\
"           and will just end up as strings.\n", argv[0], argv[0]);

#define MODE_STD   0
#define MODE_LIB   1
#define MODE_RUN   2
#define MODE_GRAPH 3

void build(i8 mode, std::vector<std::string> args);

int main(int argc, const char **argv) {

	#ifdef OS_WINDOWS
		// Enable ANSI colorcodes in conhost.exe
		//SetConsoleMode(GetStdHandle(STD_OUTPUT_HANDLE), ENABLE_VIRTUAL_TERMINAL_PROCESSING);
		//SetConsoleMode(GetStdHandle(STD_ERROR_HANDLE),  ENABLE_VIRTUAL_TERMINAL_PROCESSING);
	#endif

	std::vector<std::string> args(argv, argv+argc);

	if(argc < 2 || (argc == 2 && args[1] == "help")) {
		_usage();
	} else if(argc >= 3) {
		if(args[1] == "build") {
			build(MODE_STD, args);
		} else if(args[1] == "build_lib") {
			build(MODE_LIB, args);
		} else if(args[1] == "run") {
			build(MODE_RUN, args);
		} else if(args[1] == "ast_graph") {
			build(MODE_GRAPH, args);
		} else {
			printf_red("Unrecognized option \"%s\"!\n\n", argv[1]);
			_usage()
			return 1;
		}
	} else {
		print_red("Unrecognized options!\n\n");
		_usage();
		return 1;
	}
}

bool starts_with(string haystack, string needle) {
	if(needle.size() > haystack.size()) return false;
	return haystack.substr(0, needle.size()) == haystack;
}

// Not sure if I want to complicate things this much
// There's no reason to not just add parameters and call with a shell script
#if 0

#define build_cfg_types \
	FIELD(main_file); \
	FIELD()

struct BuildConst {
	string name, val;
}
struct BuildCfg {
	std::vector<BuildConst> build_constants;
}

// Literally only called once, so why not inline it and save a function call?
force_inline parse_build_cfg() {
	
	string cfg = read_file(make_path("build.cfg"));

	int line = 1;

	for(int i=0; i < cfg.size(); ++i) {
		char c = cfg[i];
		if(c == '\n') {
			++line
			continue;
		} else if(is_whitespace(c)) {
			continue;
		} else if(c == '#' && blank_line) {
			for(;i < cfg.size(); ++i) {
				if(cfg[i] == '\n') {
					++line
					goto pbc_loop_end;
				}
			}
		} else {
			string prop_name = "";
			for(;i < cfg.size(); ++i) {
				if(cfg[i] == ' ') {
					continue;
				}
				if(cfg[i] == ':' && cfg[i+1] == '=') {
					i+=2;
					break;
				}
				prop_name += cfg[i];
			}
			string prop_val = "";
			for(;i < cfg.size(); ++i) {
				if(cfg[i] == ' ') {
					continue;
				}
				if(cfg[i] == '#') {
					for(;i < cfg.size(); ++i) {
						if(cfg[i] == '\n') {
							++line;
							break;
						}
					}
				}
				if(cfg[i] == '\n') {
					++line;
				}
				prop_val += cfg[i];
			}
			#define cfg_prop(n,v) if()
		}
pbc_loop_end:
	}

}

#endif

void build(i8 build_mode, std::vector<std::string> args) {

	string file_name = "";
	string output_name = "";

	for(u32 i = 2; i < args.size(); ++i) {
		if(args[i] == "-o" || args[i] == "--output") {
			++i;

			output_name = args[i];

		} else if(args[i] == "-D" || args[i] == "--define") {
			// parse identifier args[i+1]
			// parse value args[i+2]
			i+=2;
			continue;
		} else {
			if(file_name != "") {
				print("Too many input files or arguments that I don't understand!\n");
				print_red("Offending argument: ");
				printf("\"%s\" when filename is already \"%s\"\n", args[i].c_str(), file_name.c_str());
				exit(1);
			}
			file_name = args[i];
		}
	}

	if(output_name == "") {
		print_orn("WARNING: No output name passed. Cannot automatically generate yet. TODO!\n" << __FILE__ << ": " << __LINE__ << "\n");
		exit(0);
	}

	//exit(0);

	if(file_name == "") {
		//if(fs::exists(make_path("build.cfg"))) {
			
			// Parsing a build config file.


		/*} else */ {
			print_red("No Helm source files passed!\n");
			exit(2);
		}
	}
	
	print_orn("STARTING LEXER\n");
	auto tokens = tokenize_file(make_path(file_name));
	print_orn("ENDING LEXER\n");
	
	if(fail_type == LEXER_FAIL) {
		print_red("\nFailed to lex the input. Exiting.\n");
		exit(3);
	}

	for(u32 i=0;i<tokens.size();i++) {
		print_tok(tokens[i]);
	}

	print_orn("STARTING PARSER\n");
	auto ast = parse_tokens(make_path(file_name), tokens);
	print_orn("ENDING PARSER\n");
	
	if(fail_type == PARSER_FAIL) {
		print_red("\nFailed to parse the input. Exiting.\n");
		exit(3);
	}

	print_orn("PRINTING TOP-LEVEL NODE TYPES\n");
	for(u32 i = 0; i < ast.nodes.size(); i++) {
		printf("\tKind: %s\n", NODE_TYPE_NAMES[ast.nodes[i]->kind]);
	}
	ast_graph(ast, make_path(output_name));

}