#pragma once

#include "parser.hpp"

void ast_graph(AstFile ast_file, fs::path out_file);

struct GraphvizData {
	string id;
};