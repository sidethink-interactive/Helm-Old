#pragma once

#include "util.hpp"
#include "lexer.hpp"



////////////////////////////////
//                            //
// Black Magic dwells within. //
// Enter at your own peril.   //
//                            //
////////////////////////////////

enum CallingConvention {
	CC_HELM, CC_C
};

struct AstNode;

#define AST_NODE_LIST \
	NODE(Identifier,     identifier,      "identifier", ) \
	NODE(Literal,        literal,         "literal", ) \
	NODE(Array,          array,           "array",                            AstNode *array, *expr;) \
	NODE(HashIf,         hash_if,         "#if",                              AstNode *body, *cond;) \
	NODE(HashType,       hash_type,       "#type",                            AstNode *funcPrototype;) \
	NODE(HashGlobal,     hash_global,     "#global",                          AstNode *body;) \
	NODE(HashRaw,        hash_raw,        "#raw",                             AstNode *ptr;) \
	NODE(Load,           load,            "load",                             AstNode *path, *local_name;) \
	NODE(Accessor,       accessor,        "accessor",                         AstNode *parent, *child;) \
	NODE(Return,         _return,         "return",                           std::vector<AstNode*> values;) \
	NODE(CallExpr,       call_expr,       "call expression",                  AstNode *func; std::vector<AstNode*> args;) \
	NODE(CastExpr,       cast_expr,       "cast expression",                  AstNode *type, *expr;) \
	NODE(TernaryExpr,    ternary_expr,    "ternary expression",               AstNode *cond, *left, *right;) \
	NODE(AssignStmt,     assign_stmt,     "assign statement",                 std::vector<AstNode*> left, right; AstNode *type; bool creation, constant;) \
	NODE(IncrDecrStmt,   incr_decr_stmt,  "increment or decrement statement", AstNode *name; bool isPrefix;) \
	NODE(Ellipsis,       ellipsis,        "ellipsis expansion",               AstNode *left, *right;) \
	NODE(UnaryExpr,      unary_expr,      "unary expression",                 AstNode *expr;) \
	NODE(BinaryExpr,     binary_expr,     "binary expression",                AstNode *left, *right;) \
	NODE(Block,          block,           "block",                            std::vector<AstNode*> stmts;) \
	NODE(Conditional,    conditional,     "conditional",                      std::vector<AstNode*> cases;) \
	NODE(If,             _if,             "if",                               AstNode *body, *cond;) \
	NODE(For,            _for,            "for",                              AstNode *declaration, *cond, *step, *body;) \
	NODE(ForIn,          for_in,          "for in",                           AstNode *iter, *value, *list, *body;) \
	NODE(FuncLiteral,    func_literal,    "function literal",                 AstNode *foreign_name, *foreign_lib, *body; \
	                                                                                std::vector<AstNode*> in, declTypes, out; CallingConvention* cc;) \
	NODE(Structure,      structure,       "structure",                        AstNode* body;) \
	NODE(ImplicitStruct, implicit_struct, "implicit struct",                  std::vector<AstNode*> members;) \
	NODE(ImplicitArray,  implicit_array,  "implicit array",                   std::vector<AstNode*> values;) \
	/* End of node list */


enum AstNodeKind {
#define NODE(name, var_name, str, ...) NodeKind_ ## name,
AST_NODE_LIST
#undef NODE
AstNodeKind_Count
};

#define NODE(name, var_name, str, ...) struct AstNode_ ## name {Token token; __VA_ARGS__ };
AST_NODE_LIST
#undef NODE

#ifndef PARSER_INTERNAL
extern
#endif
const char* NODE_TYPE_NAMES[]
#ifdef PARSER_INTERNAL
 = {
	#define NODE(name, var_name, str, ...) str,
	AST_NODE_LIST
	#undef NODE
	"unknown node type"
}
#endif
;

#ifndef PARSER_INTERNAL
extern
#endif
const char* NODE_TYPE_NAMES_SNAKE[]
#ifdef PARSER_INTERNAL
 = {
 	#define _ANL_stringify(var_name) #var_name
	#define NODE(name, var_name, str, ...) _ANL_stringify(var_name),
	AST_NODE_LIST
	#undef NODE
	#undef _ANL_stringify
	"unknown node type"
}
#endif
;

#define NODE_TYPE_NAME(node) string(NODE_TYPE_NAMES[node->kind])

struct GraphvizData;
struct TypecheckData;
struct EntityData;

struct AstNode__BinOutData {
	TypecheckData* type_data;
	EntityData* entity_data;
};

struct AstNode {
	AstNodeKind kind;
	union {
		#define NODE(name, var_name, str, ...) AstNode_ ## name *var_name;
		AST_NODE_LIST;
		#undef NODE
	};
	union {
		AstNode__BinOutData compiler_data;
		GraphvizData* graphviz_data;
	};
};

#if !defined(PARSER_INTERNAL) && !defined(EXPORT_NODE_LIST)
#undef AST_NODE_LIST
#endif