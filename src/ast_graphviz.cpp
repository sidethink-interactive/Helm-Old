#define EXPORT_NODE_LIST
#include "ast_graphviz.hpp"
#include "util.hpp"

struct GraphvizContext {
	int last_id;
	string file;
};

#define make_id(node) (string("graph_")+NODE_TYPE_NAMES_SNAKE[node->kind]+string("_id_")+std::to_string(c->last_id++))

void graph_node(GraphvizContext* c, AstNode* node);

string escape(string in) {
	string out;
	for(u32 i=0;i<in.length();i++) {
		char c = in[i];
		if(c == '\"') out += "\\\"";
		else out += c;
	}
	return out;
}




void graph_cast_expr     (GraphvizContext* c, AstNode* node) {
	print_red("graph_call_expr UNIMPLEMENTED\n");
	exit(1);
}
void graph_ternary_expr  (GraphvizContext* c, AstNode* node) {
	print_red("graph_ternary_expr UNIMPLEMENTED\n");
	exit(1);

	// shape=diamond
}




// Baby Blue
void graph_identifier    (GraphvizContext* c, AstNode* node) {
	c->file += node->graphviz_data->id + " [shape=box,style=filled,color=\"#c0cde0\",label=\"ID | " + escape(node->identifier->token.text) + "\"];\n";
}

// Light Green
void graph_literal       (GraphvizContext* c, AstNode* node) {
	c->file += node->graphviz_data->id + " [shape=box,style=filled,color=\"#a4d8c6\",label=\"LIT | " + escape(node->literal->token.text) + "\"];\n";
}

// Light red/gray
// array | green line
// expr  | blue line
void graph_array   (GraphvizContext* c, AstNode* node) {

	c->file += node->graphviz_data->id + " [shape=box,style=filled,color=\"#ead0d6\",label=\"ARRAY | OP=" + node->array->token.text + "\"];\n";

	graph_node(c, node->array->array);
	c->file += node->graphviz_data->id + " -> " + node->array->array->graphviz_data->id + " [color=\"green\"];\n";

	graph_node(c, node->array->expr);
	c->file += node->graphviz_data->id + " -> " + node->array->expr->graphviz_data->id + " [color=\"blue\"];\n";
}

// Light red/gray
// cond  | green line
// body  | blue line
void graph_hash_if   (GraphvizContext* c, AstNode* node) {

	c->file += node->graphviz_data->id + " [shape=parallelogram,style=filled,color=\"#ead0d6\",label=\"#IF | OP=" + node->hash_if->token.text + "\"];\n";

	graph_node(c, node->hash_if->cond);
	c->file += node->graphviz_data->id + " -> " + node->hash_if->cond->graphviz_data->id + " [color=\"green\"];\n";

	graph_node(c, node->hash_if->body);
	c->file += node->graphviz_data->id + " -> " + node->hash_if->body->graphviz_data->id + " [color=\"blue\"];\n";
}

// Baby Blue
// funcPrototype | green line
void graph_hash_type   (GraphvizContext* c, AstNode* node) {

	c->file += node->graphviz_data->id + " [shape=parallelogram,style=filled,color=\"#ead0d6\",label=\"#TYPE | OP=" + node->hash_type->token.text + "\"];\n";

	graph_node(c, node->hash_type->funcPrototype);
	c->file += node->graphviz_data->id + " -> " + node->hash_type->funcPrototype->graphviz_data->id + " [color=\"green\"];\n";
}

// Light red/gray
// body   | blue line
void graph_hash_global   (GraphvizContext* c, AstNode* node) {

	c->file += node->graphviz_data->id + " [shape=parallelogram,style=filled,color=\"#ead0d6\",label=\"#GLOBAL | OP=" + node->hash_global->token.text + "\"];\n";

	graph_node(c, node->hash_global->body);
	c->file += node->graphviz_data->id + " -> " + node->hash_global->body->graphviz_data->id + " [color=\"blue\"];\n";
}

// Light red/gray
// ptr  | green line
void graph_hash_raw  (GraphvizContext* c, AstNode* node) {

	c->file += node->graphviz_data->id + " [shape=parallelogram,style=filled,color=\"#ead0d6\",label=\"#RAW | OP=" + node->hash_raw->token.text + "\"];\n";

	graph_node(c, node->hash_raw->ptr);
	c->file += node->graphviz_data->id + " -> " + node->hash_raw->ptr->graphviz_data->id + " [color=\"green\"];\n";
}

// Light red/gray
// path       | green line
// local_name | blue line
void graph_load   (GraphvizContext* c, AstNode* node) {

	c->file += node->graphviz_data->id + " [shape=parallelogram,style=filled,color=\"#ead0d6\",label=\"LOAD | OP=" + node->load->token.text + "\"];\n";

	graph_node(c, node->load->path);
	c->file += node->graphviz_data->id + " -> " + node->load->path->graphviz_data->id + " [color=\"green\"];\n";

	if (node->load->local_name != nullptr) {

		graph_node(c, node->load->local_name);
		c->file += node->graphviz_data->id + " -> " + node->load->local_name->graphviz_data->id + " [color=\"blue\"];\n";
	}
}

// Ugly gray-ish green
// Green | parent
// Gray  | child
void graph_accessor (GraphvizContext* c, AstNode* node) {

	c->file += node->graphviz_data->id + " [shape=parallelogram,style=filled,color=\"#8db582\",label=\"ACCESSOR\"];\n";
	graph_node(c, node->accessor->parent);
	c->file += node->graphviz_data->id + " -> " + node->accessor->parent->graphviz_data->id + " [color=\"green\"];\n";
	graph_node(c, node->accessor->child);
	c->file += node->graphviz_data->id + " -> " + node->accessor->child->graphviz_data->id + " [color=\"gray\"];\n";
}

// Pink
// values | green line
void graph__return   (GraphvizContext* c, AstNode* node) {

	c->file += node->graphviz_data->id + " [shape=parallelogram,style=filled,color=\"#ef7ac8\",label=\"RETURN\"];\n";

	for(u32 i=0; i<node->_return->values.size(); i++) {
		graph_node(c, node->_return->values[i]);

		c->file += node->graphviz_data->id + " -> " + node->_return->values[i]->graphviz_data->id + " [color=\"green\"];\n";
	}
}

// Pink
// Function name | green line
// Arguments     | blue line
void graph_call_expr   (GraphvizContext* c, AstNode* node) {

	string name = "CALL EXPRESSION";

	c->file += node->graphviz_data->id + " [shape=parallelogram,style=filled,color=\"#ef7ac8\",label=\"" + name + "\"];\n";

	graph_node(c, node->call_expr->func);
	c->file += node->graphviz_data->id + " -> " + node->call_expr->func->graphviz_data->id + " [color=\"green\"];\n";

	for(u32 i=0; i<node->call_expr->args.size(); i++) {
		graph_node(c, node->call_expr->args[i]);

		c->file += node->graphviz_data->id + " -> " + node->call_expr->args[i]->graphviz_data->id + " [color=\"blue\"];\n";
	}
}

// Pink
// Variable side | green line
// Type          | red line
// Value side    | blue line
void graph_assign_stmt   (GraphvizContext* c, AstNode* node) {

	string name = "ASSIGN | CREATION(" + string(node->assign_stmt->creation?"T":"F") + "), CONST(" + string(node->assign_stmt->constant?"T":"F") + ")";

	c->file += node->graphviz_data->id + " [shape=parallelogram,style=filled,color=\"#ef7ac8\",label=\"" + name + "\"];\n";

	for(u32 i=0; i < node->assign_stmt->left.size(); i++) {

		if (node->assign_stmt->left[i]->kind == NodeKind_Identifier) {

			std::cout << "\nMy token is: " + node->assign_stmt->left[i]->identifier->token.text + "\n";
		}

		graph_node(c, node->assign_stmt->left[i]);
		c->file += node->graphviz_data->id + " -> " + node->assign_stmt->left[i]->graphviz_data->id + " [color=\"green\"];\n";
	}

		//debug
		std::cout << "\nleft is finished!\n\n";

	if (node->assign_stmt->type != nullptr) {
		graph_node(c, node->assign_stmt->type);
		c->file += node->graphviz_data->id + " -> " + node->assign_stmt->type->graphviz_data->id + " [color=\"red\"];\n";
	}

	for(u32 i=0; i < node->assign_stmt->right.size(); i++) {
		graph_node(c, node->assign_stmt->right[i]);
		c->file += node->graphviz_data->id + " -> " + node->assign_stmt->right[i]->graphviz_data->id + " [color=\"blue\"];\n";
	}
}


// Ugly gray-ish green
// name | green line
void graph_incr_decr_stmt(GraphvizContext* c, AstNode* node) {

	string name = string(node->incr_decr_stmt->token.text == "++" ? "INCR" : "DECR") + ", PREFIX=" + string(node->incr_decr_stmt->isPrefix ? "TRUE" : "FALSE");

	c->file += node->graphviz_data->id + " [shape=parallelogram,style=filled,color=\"#8db582\",label=\"" + name + "\"];\n";

	graph_node(c, node->incr_decr_stmt->name);
	c->file += node->graphviz_data->id + " -> " + node->incr_decr_stmt->name->graphviz_data->id + " [color=\"green\"];\n";
}


// Ugly gray-ish green
// left  | blue line
// right | red line
void graph_ellipsis(GraphvizContext* c, AstNode* node) {

	c->file += node->graphviz_data->id + " [shape=parallelogram,style=filled,color=\"#8db582\",label=\"ELLIPSIS\"];\n";

	if (node->ellipsis->left != nullptr) {

		graph_node(c, node->ellipsis->left);
		c->file += node->graphviz_data->id + " -> " + node->ellipsis->left->graphviz_data->id + " [color=\"blue\"];\n";
	}

	graph_node(c, node->ellipsis->right);
	c->file += node->graphviz_data->id + " -> " + node->ellipsis->right->graphviz_data->id + " [color=\"red\"];\n";
}

void graph_unary_expr    (GraphvizContext* c, AstNode* node) {

	c->file += node->graphviz_data->id + " [shape=parallelogram,style=filled,color=\"#ead0d6\",label=\"Unary | OP=" + node->unary_expr->token.text + "\"];\n";

	graph_node(c, node->unary_expr->expr);
	c->file += node->graphviz_data->id + " -> " + node->unary_expr->expr->graphviz_data->id + " [color=\"green\"];\n";

}

// Light red/gray
// Left  | green line
// Right | blue line
void graph_binary_expr   (GraphvizContext* c, AstNode* node) {

	c->file += node->graphviz_data->id + " [shape=parallelogram,style=filled,color=\"#ead0d6\",label=\"BIN EXPR | OP=" + node->binary_expr->token.text + "\"];\n";
	graph_node(c, node->binary_expr->left);
	c->file += node->graphviz_data->id + " -> " + node->binary_expr->left->graphviz_data->id + " [color=\"green\"];\n";
	graph_node(c, node->binary_expr->right);
	c->file += node->graphviz_data->id + " -> " + node->binary_expr->right->graphviz_data->id + " [color=\"gray\"];\n";

}

// Light red/gray
// Statements  | green line
void graph_block   (GraphvizContext* c, AstNode* node) {

	c->file += node->graphviz_data->id + " [shape=parallelogram,style=filled,color=\"#ead0d6\",label=\"BLOCK | OP=" + node->block->token.text + "\"];\n";


	for(u32 i=0; i<node->block->stmts.size(); i++) {

		graph_node(c, node->block->stmts[i]);
		c->file += node->graphviz_data->id + " -> " + node->block->stmts[i]->graphviz_data->id + " [color=\"green\"];\n";
	}
}

// Light red/gray
// cases  | green line
void graph_conditional   (GraphvizContext* c, AstNode* node) {

	c->file += node->graphviz_data->id + " [shape=parallelogram,style=filled,color=\"#ead0d6\",label=\"CONDITIONAL| OP=" + node->conditional->token.text + "\"];\n";


	for(u32 i = 0; i < node->conditional->cases.size(); i++) {

		graph_node(c, node->conditional->cases[i]);
		c->file += node->graphviz_data->id + " -> " + node->conditional->cases[i]->graphviz_data->id + " [color=\"green\"];\n";
	}
}

// Light red/gray
// cond  | green line
// body  | blue line
void graph__if   (GraphvizContext* c, AstNode* node) {

	c->file += node->graphviz_data->id + " [shape=parallelogram,style=filled,color=\"#ead0d6\",label=\"IF | OP=" + node->_if->token.text + "\"];\n";

	graph_node(c, node->_if->cond);
	c->file += node->graphviz_data->id + " -> " + node->_if->cond->graphviz_data->id + " [color=\"green\"];\n";

	graph_node(c, node->_if->body);
	c->file += node->graphviz_data->id + " -> " + node->_if->body->graphviz_data->id + " [color=\"blue\"];\n";
}

// Light red/gray
// declaration   | red line
// cond          | green line
// step          | blue line
// body          | blue line
void graph__for   (GraphvizContext* c, AstNode* node) {

	c->file += node->graphviz_data->id + " [shape=parallelogram,style=filled,color=\"#ead0d6\",label=\"FOR | OP=" + node->_for->token.text + "\"];\n";

	if (node->_for->declaration != nullptr) {

		graph_node(c, node->_for->declaration);
		c->file += node->graphviz_data->id + " -> " + node->_for->declaration->graphviz_data->id + " [color=\"red\"];\n";
	}

	graph_node(c, node->_for->cond);
	c->file += node->graphviz_data->id + " -> " + node->_for->cond->graphviz_data->id + " [color=\"green\"];\n";

	if (node->_for->step != nullptr) {

		graph_node(c, node->_for->step);
		c->file += node->graphviz_data->id + " -> " + node->_for->step->graphviz_data->id + " [color=\"blue\"];\n";
	}

	graph_node(c, node->_for->body);
	c->file += node->graphviz_data->id + " -> " + node->_for->body->graphviz_data->id + " [color=\"blue\"];\n";
}

// Light red/gray
// iter  | green line
// value | blue line
// list  | red line
// body  | blue line
void graph_for_in   (GraphvizContext* c, AstNode* node) {

	c->file += node->graphviz_data->id + " [shape=parallelogram,style=filled,color=\"#ead0d6\",label=\"FOR IN | OP=" + node->for_in->token.text + "\"];\n";

	if (node->for_in->iter != nullptr) {

		graph_node(c, node->for_in->iter);
		c->file += node->graphviz_data->id + " -> " + node->for_in->iter->graphviz_data->id + " [color=\"green\"];\n";
	}

	graph_node(c, node->for_in->value);
	c->file += node->graphviz_data->id + " -> " + node->for_in->value->graphviz_data->id + " [color=\"blue\"];\n";

	graph_node(c, node->for_in->list);
	c->file += node->graphviz_data->id + " -> " + node->for_in->list->graphviz_data->id + " [color=\"red\"];\n";

	graph_node(c, node->for_in->body);
	c->file += node->graphviz_data->id + " -> " + node->for_in->body->graphviz_data->id + " [color=\"blue\"];\n";
}

// Light red/gray
// in           | green line
// declTypes    | green line
// out          | blue line
// body         | green line
// foreign_name | gray line
// foreign_lib  | gray line
// cc           | blue line
void graph_func_literal  (GraphvizContext* c, AstNode* node) {

	c->file += node->graphviz_data->id + " [shape=parallelogram,style=filled,color=\"#ead0d6\",label=\"FUNC LITERAL | OP=" + node->func_literal->token.text + "\"];\n";

	for(u32 i=0; i<node->func_literal->in.size(); i++) {

		graph_node(c, node->func_literal->in[i]);
		c->file += node->graphviz_data->id + " -> " + node->func_literal->in[i]->graphviz_data->id + " [color=\"green\"];\n";
	}

	for(u32 i=0; i<node->func_literal->declTypes.size(); i++) {

		std::cout << "Size: " << node->func_literal->declTypes.size();

		graph_node(c, node->func_literal->declTypes[i]);
		c->file += node->graphviz_data->id + " -> " + node->func_literal->declTypes[i]->graphviz_data->id + " [color=\"green\"];\n";
	}

	for(u32 i=0; i<node->func_literal->out.size(); i++) {

		graph_node(c, node->func_literal->out[i]);
		c->file += node->graphviz_data->id + " -> " + node->func_literal->out[i]->graphviz_data->id + " [color=\"blue\"];\n";
	}

	if (node->func_literal->body != nullptr) {

		graph_node(c, node->func_literal->body);
		c->file += node->graphviz_data->id + " -> " + node->func_literal->body->graphviz_data->id + " [color=\"green\"];\n";
	}


	if(node->func_literal->foreign_name != nullptr) {

		graph_node(c, node->func_literal->foreign_name);
		c->file += node->graphviz_data->id + " -> " + node->func_literal->foreign_name->graphviz_data->id + " [color=\"gray\"];\n";
	}

	if(node->func_literal->foreign_lib != nullptr) {

		graph_node(c, node->func_literal->foreign_lib);
		c->file += node->graphviz_data->id + " -> " + node->func_literal->foreign_lib->graphviz_data->id + " [color=\"gray\"];\n";
	}

	//graph_node(c, node->func_literal->cc);
	//c->file += node->graphviz_data->id + " -> " + node->func_literal->cc->graphviz_data->id + " [color=\"gray\"];\n";
}

// Light red/gray
// body  | green line
void graph_structure   (GraphvizContext* c, AstNode* node) {

	c->file += node->graphviz_data->id + " [shape=parallelogram,style=filled,color=\"#ead0d6\",label=\"STRUCTURE | OP=" + node->structure->token.text + "\"];\n";

	graph_node(c, node->structure->body);
	c->file += node->graphviz_data->id + " -> " + node->structure->body->graphviz_data->id + " [color=\"green\"];\n";
}

// Light red/gray
// members | blue line
void graph_implicit_struct   (GraphvizContext* c, AstNode* node) {

	c->file += node->graphviz_data->id + " [shape=box,style=filled,color=\"#ead0d6\",label=\"IMPLICIT STRUCT | OP=" + node->implicit_struct->token.text + "\"];\n";


	for(u32 i=0; i<node->implicit_struct->members.size(); i++) {

		graph_node(c, node->implicit_struct->members[i]);
		c->file += node->graphviz_data->id + " -> " + node->implicit_struct->members[i]->graphviz_data->id + " [color=\"blue\"];\n";
	}
}

// Light red/gray
// values | green line
void graph_implicit_array   (GraphvizContext* c, AstNode* node) {

	c->file += node->graphviz_data->id + " [shape=box,style=filled,color=\"#ead0d6\",label=\"IMPLICIT ARRAY | OP=" + node->implicit_array->token.text + "\"];\n";

	std::cout << "\n\nFound an implicit array!\narray size is: " + std::to_string(node->implicit_array->values.size()) + "\n\n";

	for(u32 i = 0; i < node->implicit_array->values.size(); i++) {

		graph_node(c, node->implicit_array->values[i]);
		c->file += node->graphviz_data->id + " -> " + node->implicit_array->values[i]->graphviz_data->id + " [color=\"green\"];\n";
	}
}



void graph_node(GraphvizContext* c, AstNode* node) {

	printf_red("Nodeptr: %p", node);

	if(node == nullptr) {
		print_orn("Oh no. A node is nullptr!\n" << __FILE__ << ": " << __LINE__ << "\n");
	}
	
	GraphvizData* data = new GraphvizData;
	data->id = make_id(node);
	node->graphviz_data = data;

	switch(node->kind) {
		#define NODE(name, var_name, str, ...) case(NodeKind_ ## name): { printf("  Node name: %s\n", NODE_TYPE_NAME(node).c_str()); graph_ ## var_name (c, node); break;}
		AST_NODE_LIST
		#undef NODE
		default: { 

			printf_orn("Bad Node ID: [0>%d || %d>%d]\n\n", node->kind, node->kind, AstNodeKind_Count-1);
			printf_red("Unrecognized AstNode type (Internal compiler error) [0>%d || %d>%d]\n\n", node->kind, node->kind, AstNodeKind_Count-1);
			print("If your code is not confidential/private, it would\n"
				  "be highly appreciated if you created an issue on this\n"
				  "compiler's gitlab page and attached the offending source file.\n\n"
				  "Sorry for the inconvenience!\n");
			exit(1);
		}
	}
}

void connect_nodes(GraphvizContext* c, AstNode* a, AstNode* b) {

}

void ast_graph(AstFile ast_file, fs::path out_file) {
	
	GraphvizContext context;
	context.last_id = 0;
	context.file = "digraph G {\n"
	               "file [shape=oval,label=\"" + display_path(ast_file.path) + "\"];\n";


	for(u32 i=0; i<ast_file.nodes.size(); i++) {
		graph_node(&context, ast_file.nodes[i]);
		context.file += "file -> " + ast_file.nodes[i]->graphviz_data->id + ";\n";
	}
	context.file += "}";

	write_file(out_file, context.file);
}