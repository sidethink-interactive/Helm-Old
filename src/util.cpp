#include "util.hpp"
#include <fstream>
#include <iostream>

bool long_paths = false;

string read_file(fs::path filename) {

	if(!fs::exists(filename)) {
		// TODO(zachary): Make this use a standard debug context.
		std::cout << "util.cpp bad debugging info!! \n";
	}

	std::ifstream ifs(filename.string());
	return string((std::istreambuf_iterator<char>(ifs)),
	              (std::istreambuf_iterator<char>()));
}

void write_file(fs::path filename, string contents) {
	std::ofstream ofs(filename.string());
	ofs << contents;
	ofs.close();
}

fs::path get_relative_file(fs::path first_file, string second_file) {
	return fs::system_complete(first_file.parent_path() / second_file);
}

fs::path make_path(string path) {
	return fs::system_complete(fs::path(path));
}

string display_path(string path) {
	if(long_paths) return make_path(path).string();
	return fs::relative(make_path(path), fs::current_path()).string();
}

string display_path(fs::path path) {
	if(long_paths) return fs::system_complete(path).string();
	return fs::relative(path, fs::current_path()).string();
}

#define digits(n) ((n<10?1:n<100?2:n<1000?3:4) + 1)
#define ln(n) printf("\033[100m%*d \033[m ", digits(line), n)
force_inline void show_context(File* file, u32 line, u32 col) {
	if(line > 1) {
		ln(line-1);
		print(file->lines[line-2] << "\n");
	}
	ln(line);
	print(file->lines[line-1] << "\n");
	printf_orn("%*c\n", digits(line)+2+col, '^');
}
force_inline void error_at(File* file, u32 line, u32 col, string msg, CompileFailType ft) {
	print("\033[1m" << display_path(file->path) << ":" << line << ":" << col << ": ");
	print_red("error: ");
	print(msg << "\033[21m\n");
	show_context(file, line, col);
	fail_type = ft;
}
force_inline void warn_at(File* file, u32 line, u32 col, string msg) {
	print("\033[1m" << display_path(file->path) << ":" << line << ":" << col << ": ");
	print_orn("warning: ");
	print(msg << "\033[21m\n");
	show_context(file, line, col);
}