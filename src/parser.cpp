
///////////////////////////////////////////////////////////////////////////////////////
//                                                                                   //
// Brendan: This file is fairly dense, and as such has pretty verbose comments.      //
//          If you don't fully understand something, make sure you give all of       //
//          the comments a look. I did my best to make this as readable as possible, //
//          but it can still be difficult if you don't already understand what's     //
//          going on. Proceed at your own risk.                                      //
//                                                                                   //
///////////////////////////////////////////////////////////////////////////////////////


#define PARSER_INTERNAL
#include "parser.hpp"

#define parser_err(tok, ...) error_at(tok.pos.file, tok.pos.line, tok.pos.column, string(__VA_ARGS__), PARSER_FAIL)
#define parser_wrn(tok, ...) warn_at (tok.pos.file, tok.pos.line, tok.pos.column, string(__VA_ARGS__))


force_inline_all AstNode* create_node(AstNodeKind kind, Token tok) {

	AstNode *n = new AstNode;
	n->kind = kind;
	switch(kind) {
		#define NODE(name, var_name, str, ...) case NodeKind_ ## name: {n->var_name = new AstNode_ ## name; break;}
		AST_NODE_LIST
		#undef NODE
		default: {
			parser_err(tok, "INTERNAL ERROR: Trying to call `create_node` with an invalid AstNodeKind");
		}
	}
	return n;
}

force_inline_all AstNode* create_node_identifier(Token tok) {
	AstNode *n = create_node(NodeKind_Identifier, tok);
	n->identifier->token = tok;
	return n;
}

force_inline_all AstNode* create_node_literal(Token tok) {
	AstNode *n = create_node(NodeKind_Literal, tok);
	n->literal->token = tok;
	return n;
}

force_inline_all AstNode* create_node_array(Token tok, AstNode* array, AstNode* expr) {
	AstNode *n = create_node(NodeKind_Array, tok);
	n->array->token = tok;
	n->array->array = array;
	n->array->expr = expr;
	return n;
}

force_inline_all AstNode* create_node_hash_if(Token tok, AstNode* cond, AstNode* body) {
	AstNode *n = create_node(NodeKind_HashIf, tok);
	n->hash_if->token = tok;
	n->hash_if->cond = cond;
	n->hash_if->body = body;
	return n;
}

force_inline_all AstNode* create_node_hash_type(Token tok, AstNode* funcPrototype) {
	AstNode *n = create_node(NodeKind_HashType, tok);
	n->hash_type->token = tok;
	n->hash_type->funcPrototype = funcPrototype;
	return n;
}

force_inline_all AstNode* create_node_hash_global(Token tok, AstNode* body) {
	AstNode *n = create_node(NodeKind_HashGlobal, tok);
	n->hash_global->token = tok;
	n->hash_global->body = body;
	return n;
}

force_inline_all AstNode* create_node_hash_raw(Token tok, AstNode* ptr) {
	AstNode *n = create_node(NodeKind_HashRaw, tok);
	n->hash_raw->token = tok;
	n->hash_raw->ptr = ptr;
	return n;
}

force_inline_all AstNode* create_node_load(Token tok, AstNode* path, AstNode* local_name = nullptr) {
	AstNode *n = create_node(NodeKind_Load, tok);
	n->load->token = tok;
	n->load->path = path;
	n->load->local_name = local_name;
	return n;
}

force_inline_all AstNode* create_node_accessor(Token tok, AstNode *parent, AstNode *child) {
	AstNode *n = create_node(NodeKind_Accessor, tok);
	n->accessor->token = tok;
	n->accessor->parent = parent;
	n->accessor->child = child;
	return n;
}

force_inline_all AstNode* create_node__return(Token tok, std::vector<AstNode*> values) {
	AstNode *n = create_node(NodeKind_Return, tok);
	n->_return->token = tok;
	n->_return->values = values;
	return n;
}

force_inline_all AstNode* create_node_call_expr(Token tok, AstNode* func, std::vector<AstNode*> args) {
	AstNode *n = create_node(NodeKind_CallExpr, tok);
	n->call_expr->token = tok;
	n->call_expr->func = func;
	n->call_expr->args = args;
	return n;
}

force_inline_all AstNode* create_node_assign_stmt(Token tok, std::vector<AstNode*> left, std::vector<AstNode*> right, AstNode* type = nullptr, bool creation = false, bool constant = false) {
	AstNode *n = create_node(NodeKind_AssignStmt, tok);
	n->assign_stmt->token = tok;
	n->assign_stmt->left = left;
	n->assign_stmt->right = right;
	n->assign_stmt->type = type;
	n->assign_stmt->creation = creation;
	n->assign_stmt->constant = constant;
	return n;
}

force_inline_all AstNode* create_node_incr_decr_stmt(Token tok, AstNode* name, bool isPrefix) {
	AstNode *n = create_node(NodeKind_IncrDecrStmt, tok);
	n->incr_decr_stmt->token = tok;
	n->incr_decr_stmt->name = name;
	n->incr_decr_stmt->isPrefix = isPrefix;
	return n;
}

force_inline_all AstNode* create_node_ellipsis(Token tok, AstNode* left, AstNode* right) {
	AstNode *n = create_node(NodeKind_Ellipsis, tok);
	n->ellipsis->token = tok;
	n->ellipsis->left = left;
	n->ellipsis->right = right;
	return n;
}

force_inline_all AstNode* create_node_unary_expr(Token tok, AstNode* expr) {
	AstNode *n = create_node(NodeKind_UnaryExpr, tok);
	n->unary_expr->token = tok;
	n->unary_expr->expr = expr;
	return n;
}

force_inline_all AstNode* create_node_binary_expression(Token operation, AstNode* left, AstNode* right) {
	AstNode *n = create_node(NodeKind_BinaryExpr, operation);
	n->binary_expr->token = operation;
	n->binary_expr->left = left;
	n->binary_expr->right = right;
	return n;
}

force_inline_all AstNode* create_node_block(Token tok, std::vector<AstNode*> stmts) {
	AstNode *n = create_node(NodeKind_Block, tok);
	n->block->token = tok;
	n->block->stmts = stmts;
	return n;
}

force_inline_all AstNode* create_node_conditional(Token tok, std::vector<AstNode*> cases) {
	AstNode *n = create_node(NodeKind_Conditional, tok);
	n->conditional->token = tok;
	n->conditional->cases = cases;
	return n;
}

force_inline_all AstNode* create_node__if(Token tok, AstNode* cond, AstNode* body) {
	AstNode *n = create_node(NodeKind_If, tok);
	n->_if->token = tok;
	n->_if->cond = cond;
	n->_if->body = body;
	return n;
}

force_inline_all AstNode* create_node__for(Token tok, AstNode* declaration, AstNode* cond, AstNode* step, AstNode* body) {
	AstNode *n = create_node(NodeKind_For, tok);
	n->_for->token = tok;
	n->_for->declaration = declaration;
	n->_for->cond = cond;
	n->_for->step = step;
	n->_for->body = body;
	return n;
}

force_inline_all AstNode* create_node_for_in(Token tok, AstNode* iter, AstNode* value, AstNode* list, AstNode* body) {
	AstNode *n = create_node(NodeKind_ForIn, tok);
	n->for_in->token = tok;
	n->for_in->iter = iter;
	n->for_in->value = value;
	n->for_in->list = list;
	n->for_in->body = body;
	return n;
}

force_inline_all AstNode* create_node_func_literal(Token tok, std::vector<AstNode*> in, std::vector<AstNode*> declTypes, std::vector<AstNode*> out,
	AstNode* body = nullptr, AstNode* foreign_name = nullptr, AstNode* foreign_lib = nullptr, CallingConvention* cc = nullptr) {

	AstNode *n = create_node(NodeKind_FuncLiteral, tok);
	n->func_literal->in = in;
	n->func_literal->declTypes = declTypes;
	n->func_literal->out = out;
	n->func_literal->body = body;
	n->func_literal->foreign_name = foreign_name;
	n->func_literal->foreign_lib = foreign_lib;
	n->func_literal->cc = cc;
	return n;
}

force_inline_all AstNode* create_node_structure(Token tok, AstNode* body) {
	AstNode *n = create_node(NodeKind_Structure, tok);
	n->structure->token = tok;
	n->structure->body = body;
	return n;
}

force_inline_all AstNode* create_node_implicit_struct(Token tok, std::vector<AstNode*> members) {
	AstNode *n = create_node(NodeKind_ImplicitStruct, tok);
	n->implicit_struct->token = tok;
	n->implicit_struct->members = members;
	return n;
}

force_inline_all AstNode* create_node_implicit_array(Token tok, std::vector<AstNode*> values) {
	AstNode *n = create_node(NodeKind_ImplicitArray, tok);
	n->implicit_array->token = tok;
	n->implicit_array->values = values;
	return n;
}



std::vector<AstNode*> parentBlocks = std::vector<AstNode*>();

AstNode* currentBlock = nullptr;



AstFile parse_tokens(fs::path path, std::vector<Token> tokens) {

	AstFile file{path, tokens};

	currentBlock = create_node_block(tokens[0], std::vector<AstNode*>());


	// NOTE(Brendan): This is used very rarely. It's set to true when a block of only a single line is needed and braces are unnecessary. For example:
	// #global
	// my_var := 0;
	// my_var would be in its own block (which belongs to #global).
	bool singleLineBlock = false;


	AstNode* activeBlock = currentBlock;



	// NOTE(Brendan): THe EOF token should always come at the end of the file. (Shocking and strange, I know.)
	// If the EOF is _NOT_ at the end of the token vector, we have some serious issues.
	if (tokens[tokens.size() - 1].kind != Token_EOF) {

		parser_err(tokens[0], "Parser failed to begin; the token vector passed is not terminated by an EOF token!");

		return file;
	}


	// NOTE(Brendan): floating_identifiers is a list of identifiers that have yet to have any sort of operation performed on them.
	// For example: 
	// var := 7;
	// var would be added to this list until we have parsed the assignment operator.
	// Another example: 
	// var1, var2 := 3, 7;
	// both var1 and var2 would be added to the list.
	std::vector<AstNode*> floating_identifiers = std::vector<AstNode*>();



	// NOTE(Brendan): First we loop through and remove all of the comments. 
	// This makes parsing remarkably easier, especially when debugging the parser.
	for (u32 index = 0; index < tokens.size() - 1; index++) {

		if (tokens[index].kind == Token_Comment) {

			tokens.erase(tokens.begin() + index);

			index--;
		}
	}


	// NOTE(Brendan): Index is incremented throughout the loop. (Typically at the end of a conditional statement.)
	// Having it increment at the start of every loop was causing... undesirable effects, so it had to be done manually.
	// It's a pain, but it works.
	for (u32 index = 0; index < tokens.size() - 1;) {

		Token token = tokens[index];

		TokenKind kind = token.kind;


		// NOTE(Brendan): Some tags that require their own block (like #global) don't require braces if you only 
		// want them to apply to the line directly after them. For example:
		// #global
		// my_var := 0;
		// my_var would be in its own block (which belongs to #global).
		if (!singleLineBlock) {

			activeBlock = currentBlock;
		}


		parser_wrn(token, "New Loop: Index is: " + token.text + " - Kind is: " + token_names[kind]);

		
		// NOTE(Brendan) It should never be the case that there are dangling identifiers when we reach the end of a line.
		// We will, however, try to continue parsing.
		if (kind == Token_Semicolon) {

			if (!floating_identifiers.empty()) {

				string names;

				for (AstNode* node : floating_identifiers) {

					names += "\"" + node->identifier->token.text + "\"\n";
				}

				parser_wrn(token, "Reached the end of a line where variables were mentioned but not used. Unused variable name(s) are:\n" + names);

				floating_identifiers.clear();
			}


			// NOTE(Brendan): As the name says, a single line block should persist for only a single line.
			if (singleLineBlock) {

				singleLineBlock = false;
			}

			// NOTE(Brendan): Since we know index currently points to a semicolon, we can advance.
			index++;

		} // NOTE(Brendan): Ending semicolon.



		// NOTE(Brendan): These keywords do more or less the same thing, so they were grouped together.
		else if (kind == Token_Import || kind == Token_Load) {

			// NOTE(Brendan): Since we know the current index is import or load, now we can check for a local name or path.
			index++;

			AstNode* name = nullptr;


			if (tokens[index].kind == Token_Identifier) {

				name = create_node_identifier(tokens[index]);

				// NOTE(Brendan): Again, now that we know what index is, move it forward to grab the path to load/import from.
				index++;
			}


			if (tokens[index].kind != Token_String) {

				parser_err(tokens[index], "Attempted to use \"" + token.text + "\" without providing a path!");

				return file;
			}


			activeBlock->block->stmts.push_back(create_node_load(token, create_node_identifier(tokens[index]), name));


			// NOTE(Brendan): The above if statement guarantees that index is a string literal here. 
			// Since we know this (and it has been parsed already) we can advance.
			index++;

		} // NOTE(Brendan): Ending import and load.



		else if (kind == Token_Increment || kind == Token_Decrement) {

			if (tokens[index + 1].kind != Token_Identifier) {

				parser_err(tokens[index], "Expected a value following token \"" + token.text + "\"; Found token \"" + tokens[index].text + "\" instead.");

				return file;
			}

			// NOTE(Brendan): Since we know index points to an incr/decr, move index so that it's looking at the value instead.
			index++;

			activeBlock->block->stmts.push_back(create_node_incr_decr_stmt(token, create_node_identifier(tokens[index]), true));


			// NOTE(Brendan): Now that we've parsed what we're incrementing or decrementing, we can advance index once again.
			index++;

		} // NOTE(Brendan): Ending increment and decrement.



		// NOTE(Brendan): First we check to see if we have an increment or decrement operator following the identifier.
		// If we do, create an incr/decr node. Otherwise, we just add the identifier to floating_identifiers.
		else if (kind == Token_Identifier) {

			kind = tokens[index + 1].kind;


			if (kind == Token_Increment || kind == Token_Decrement) {

				// NOTE(Brendan): Since we're going to actually be using the increment or decrement operator (which is the token ahead of us)
				// we want to go ahead and move index forward.
				index++;

				activeBlock->block->stmts.push_back(create_node_incr_decr_stmt(tokens[index], create_node_identifier(token), false));
			}


			// NOTE(Brendan): If this identifier is _NOT_ immediately followed by an increment/decrement operator.
			else {

				if (kind == Token_Identifier) {

					parser_err(token, "Missing a comma between tokens \"" + token.text + "\" and \"" + tokens[index + 1].text + "\". Unable to continue!");

					return file;
				}


				if (kind != Token_Comma && kind != Token_Colon && !IsAssignment(kind) && kind != Token_OpenParen) {

					parser_err(tokens[index + 1], "Unexpected token \"" + tokens[index + 1].text + "\" following identifier. Unable to continue!");

					return file;
				}


				floating_identifiers.push_back(create_node_identifier(token));
			}


			// NOTE(Brendan): The identifier has been parsed successfully, so increment index.
			index++;

		} // NOTE(Brendan): Ending identifier.



		else if (kind == Token_Comma) {

			if (floating_identifiers.empty()) {

				parser_err(token, "Unexpected token \",\" encountered during parsing. Unable to continue!");

				return file;
			}

			// NOTE(Brendan): If we're looking at a comma, just increment.
			index++;

		} // NOTE(Brendan): Ending comma.



		// NOTE(Brendan): This will (obviously) be some sort of an assignment.
		// For example: 
		// index = 7;
		// index++;
		// index *= j;
		// age, name = 4000, "Your mom";
		else if (IsAssignment(kind)) {

			AstNode* assign = parse_var_assignment(floating_identifiers, tokens, index);

			if (assign == nullptr) {

				parser_err(token, "Encountered an error while attempting to parse an assignment.");

				return file;
			}


			activeBlock->block->stmts.push_back(assign);

			floating_identifiers.clear();


			// NOTE(Brendan): Since we've finished parsing this line, index can be incremented.
			index++;

		} // NOTE(Brendan): Ending assignment.



		// NOTE(Brendan): Creating a new variable. A few examples:
		// var1 : int;
		// someString, moreString, thisIsSomeBool := "some string", "and yet more string", true;
		else if (kind == Token_Colon) {


			// NOTE(Brendan): I feel like I should be able to use the line:
			// std::vector<AstNode*> nodes = parse_var_creation(floating_identifiers, tokens, index);
			// and for the most part, it works flawlessly. However, when parsing inside structs, and *only* inside structs it would seem,
			// this code causes a segfault.

			// TODO(zachary): Look at this later

			std::vector<AstNode*> newVars = std::vector<AstNode*>();

			newVars = parse_var_creation(floating_identifiers, tokens, index);


			// NOTE(Brendan): Just error checking.
			if (newVars.empty()) {

				parser_err(token, "Encountered an error while attempting to create a new variable. If no other errors occurred before this, then the "
					"problem may be with the parser itself.");

				return file;
			}


			activeBlock->block->stmts.insert(activeBlock->block->stmts.end(), newVars.begin(), newVars.end());

			floating_identifiers.clear();

			// NOTE(Brendan): Since this line has been finished, index can advance.
			//index++;

		} // NOTE(Brendan): Ending colon.



		// NOTE(Brendan): Attempting to call a function
		else if (kind == Token_OpenParen) {

			// NOTE(Brendan): Since we *are* currently looking at the parenthesis, not the name, 
			// move index back one so we're in the right spot.
			index--;

			AstNode* funcCall = GetType(tokens, index);

			if (funcCall == nullptr) {

				parser_err(token, "Encountered an error while attempting to parse a function call. The error is likely somewhere"
				 " in the parser or lexer.");

				return file;
			}


			activeBlock->block->stmts.push_back(funcCall);

			floating_identifiers.clear();


			// NOTE(Brendan): We've finished parsing this function call so index can be incremented.
			index++;

		} // NOTE(Brendan): Ending open parenthesis.



		else if (kind == Token_Return) {

			// NOTE(Brendan): Obviously if we're not inside of a function (like just randomly in the body of the file somewhere) using return doesn't make sense.
			if (parentBlocks.empty()) {

				parser_err(token, "Attempted to call \"return\" in the top level block!");

				return file;
			}


			std::vector<AstNode*> values = std::vector<AstNode*>();


			// NOTE(Brendan): Index is still pointing to the "return" keyword, so go ahead and move it forward before beginning this parsing loop.
			//index++;


			// NOTE(Brendan): Breaks when we reach a semicolon, errors if we reach EOF.
			while (true) {

				kind = tokens[++index].kind;

				//debug
				parser_wrn(tokens[index], "Inside \"return\" parsing loop. Current token: " + tokens[index].text);


				if (kind == Token_EOF) {

					parser_err(token, "Encountered EOF while parsing return statement values!");

					return file;
				}

				else if (kind == Token_Semicolon) {

					break;
				}

				else if (kind == Token_Comma) {

					continue;
				}


				values.push_back(parse_expression(tokens, index));

				if (values.back() == nullptr) {

					parser_err(token, "Encountered an error while parsing return statement values. The error is likely somewhere"
					 " in the parser or lexer rather than the expression itself.");

					return file;
				}


				if (kind == Token_EOF) {

					parser_err(token, "Encountered EOF while parsing return statement values!");

					return file;
				}

				else if (tokens[index].kind == Token_Semicolon) {

					break;
				}

				else if (tokens[index].kind  == Token_Comma) {

					continue;
				}
			}


			activeBlock->block->stmts.push_back(create_node__return(token, values));


			// NOTE(Brendan): Return should always be the last statement in a block, so let's make sure of that.
			index++;

			if (tokens[index].kind != Token_CloseBrace) {

				parser_wrn(token, "Unreachable code has been found after the keyword \"return\"; is this intentional?");
			}

		} // NOTE(Brendan): Ending return.



		// NOTE(Brendan): Attempting to create a new block.
		else if (kind == Token_OpenBrace) {


			// NOTE(Brendan): Make sure we don't have any variables hanging around that shouldn't be.
			if (!floating_identifiers.empty()) {

				string names;

				for (AstNode* node : floating_identifiers) {

					names += "\"" + node->identifier->token.text + "\"\n";
				}

				parser_wrn(token, "A new block is being created while variables have been mentioned but not used. \nUnused variable name(s) are:\n" + names);

				floating_identifiers.clear();
			}

			// NODE(Brendan): This will create a new block. If we fail to create a new block, we error inside CreateNewBlock - no need to error here as well.
			// Just go ahead and return a failure.
			if (!CreateNewBlock(tokens[index])) {

				return file;
			}


			// NOTE(Brendan): Since this block isn't a function body or anything (and as such doesn't already have
			// something pointing to it), if we don't add ourselves to the parent block there won't be anything 
			// pointing back to this block.
			parentBlocks.back()->block->stmts.push_back(currentBlock);


			// NOTE(Brendan): Now that we've finished parsing the open brace, we don't really need to be pointing to it anymore. Increment index.
			index++;

		} // NOTE(Brendan): Ending open brace.



		// NOTE(Brendan): Attempting to close the current block.
		else if (kind == Token_CloseBrace) { 


			// NOTE(Brendan): Make sure we don't have any variables hanging around that shouldn't be.
			if (!floating_identifiers.empty()) {

				string names;

				for (AstNode* node : floating_identifiers) {

					names += "\"" + node->identifier->token.text + "\"\n";
				}

				parser_wrn(token, "The active block is being closed while variables have been mentioned but not used. Unused variable name(s) are:\n" + names);

				floating_identifiers.clear();
			}


			if (!parentBlocks.empty()) {


				// NODE(Brendan): This ends the current block. If we fail to end the block (for some reason), we error 
				// inside CloseCurrentBlock - no need to error here as well. Just go ahead and return a failure.
				if (!CloseCurrentBlock(tokens[index])) {

					return file;
				}
			}

			// NOTE(Brendan): If we are the top level block.
			else {

				parser_err(token, "Attempted to close the top level block!");

				return file;
			}


			// NOTE(Brendan): The closing brace has been parsed, so we no longer need to point to it. Go ahead and advance index.
			index++;

		} // NOTE(Brendan): Ending close brace.



		else if (kind == Token_If || kind == Token_HashIf) {

			std::vector<AstNode*> result; 


			// NOTE(Brendan): Just grabbing the correct node based on whether this is a #if or not.
			result.push_back(parse_if(tokens, index));


			if (result.back() == nullptr) {

				parser_err(token, "Encountered a null conditional statement; unable to continue!");

				return file;
			}


			activeBlock->block->stmts.push_back(create_node_conditional(token, result));


			// NOTE(Brendan): The statement has been parsed. Index can now be incremented.
			index++;

		} // NOTE(Brendan): Ending if and #if.



		else if (kind == Token_Else || kind == Token_HashElse) {


			// NOTE(Brendan): Figuring out which "if" type we should be matching with.
			TokenKind match = (kind == Token_Else) ? Token_If : Token_HashIf;


			// NOTE(Brendan): Naturally, an else is only legal directly after an if. If this is not the case, error.
			// First we check to make sure there has been a conditional statement made immediately before us, then we check to see
			// if that statement is of the correct type (if or #if) and then confirm it wasn't an 'else' itself by seeing if
			// the condition is null.
			if (activeBlock->kind != NodeKind_Conditional
				|| (match == Token_If) ? 
					activeBlock->conditional->cases.back()->kind != NodeKind_If || activeBlock->conditional->cases.back()->_if->cond == nullptr 
				  : activeBlock->conditional->cases.back()->kind != NodeKind_HashIf || activeBlock->conditional->cases.back()->hash_if->cond == nullptr) {


				parser_err(token, "Mismatched \"" + token.text + "\" statement; the keyword \"" + token.text + "\" must come immediately after a" + 
					(match == Token_If ? "n \"if\"" : " \"#if\"") + " statement.");

				return file;
			}


			// NOTE(Brendan): If we're currently on the "else" then we need to increment to check for an "if".
			index++;

			AstNode* result;


			// NOTE(Brendan): Checking for else if.
			if (tokens[index].kind == match) {

				result = parse_if(tokens, index);


				// NOTE(Brendan): Just trying to catch weird errors and edge cases.
				if (result == nullptr) {

					parser_err(token, "Encountered a null conditional statement while parsing \"else\"; unable to continue!");

					return file;
				}
			}


			// NOTE(Brendan): If this "else" is _NOT_ followed by an "if". (i.e. it has no condition.)
			else {


				// NOTE(Brendan): Helm does not support single-line else statements. All if and else statements must use braces.
				if (tokens[index].kind != Token_OpenBrace) {

					parser_err(tokens[index], "Found token \"" + tokens[index].text + "\" where token \"{\" was expected. Unable to continue!");

					return file;
				}

				// NODE(Brendan): This will create a new block. If we fail to create a new block, we error inside CreateNewBlock - no need to error here as well.
				// Just go ahead and return a failure.
				if (!CreateNewBlock(tokens[index])) {

					return file;
				}


				// NOTE(Brendan): An "else" is just an "if" with a null condition.
				result = (match == Token_If) ? create_node__if(token, currentBlock, nullptr) : create_node_hash_if(token, currentBlock, nullptr);
			}


			activeBlock->conditional->cases.push_back(result);


			// NOTE(Brendan): Everything has been parsed, so let's move on.
			index++;

		} // NOTE(Brendan): Ending else and #else.



		else if (kind == Token_For) {

			AstNode* forStmnt = parse_for(tokens, index);

			if (forStmnt == nullptr) {

				parser_err(token, "Encountered an error while parsing a for statement!");

				return file;
			}


			activeBlock->block->stmts.push_back(forStmnt);

			// NOTE(Brendan): The above function takes care of everything. ALl that we need to do is increment.
			index++;

		} // NOTE(Brendan): Ending "for".



		else if (kind == Token_HashGlobal) {

			// NOTE(Brendan): As is pretty normal, since we already know we're pointing to HashGlobal, we can increment index to see what comes next.
			index++;


			if (tokens[index].kind == Token_OpenBrace) {



				//debug
				parser_wrn(token, "Inside #global with scope.");


				// NODE(Brendan): This will create a new block. If we fail to create a new block, we error inside CreateNewBlock - no need to error here as well.
				// Just go ahead and return a failure.
				if (!CreateNewBlock(tokens[index])) {

					return file;
				}

				activeBlock->block->stmts.push_back(create_node_hash_global(token, currentBlock));
			}


			// NOTE(Brendan): If the next token is _NOT_ an open brace.
			else {



				//debug
				parser_wrn(token, "Inside #global without scope.");



				singleLineBlock = true;

				AstNode* newBlock = create_node_block(token, std::vector<AstNode*>());

				// NOTE(Brendan): This is exactly like the hash_global creation call above, with the exception that we're creating our own
				// "one line only" block.
				activeBlock->block->stmts.push_back(create_node_hash_global(token, newBlock));

				activeBlock = newBlock;
			}

			// NOTE(Brendan): Whether we have a single line scope or not, everything we needed to do here is finished now. Increment index and move along.
			index++;

		} // NOTE(Brendan): Ending #global.



		// NOTE(Brendan): If we've reached this point, we are trying to parse a token that we have no idea how to handle. All we can do is error.
		else {

			parser_err(token, "Unexpected token \"" + token.text + "\" encountered during parsing. Unable to continue!");

			return file;
		} // NOTE(Brendan): Ending error checking.

	} // NOTE(Brendan): End of parsing loop.



	// NOTE(Brendan): Checking for any open braces without a matching closing brace.
	if (!parentBlocks.empty()) {


		parser_err(currentBlock->block->token, "Parsing failed: reached the end of the file without encountering a matching closing brace!");

		for (AstNode* block : parentBlocks) {

			if (block == parentBlocks[0]) {

				continue;
			}

			parser_err(block->block->token, "Parsing failed: reached the end of the file without encountering a matching closing brace!");
		}

		return file;
	}



	file.nodes = currentBlock->block->stmts;

	return file;

} // NOTE(Brendan): Ending parse_tokens.




// NOTE(Brendan): A utility function made for the purpose of cleaning up the main loop. 
// This function handles if statements. It does everything from creating the new block to parsing the condition.
AstNode* parse_if(std::vector<Token> tokens, u32 &index) {


	// NOTE(Brendan): Just making sure we're not in the wrong place.
	if (tokens[index].kind != Token_If && tokens[index].kind != Token_HashIf) {

		parser_err(tokens[index], "Attempting to create an \"if\" statement with token \"" + tokens[index].text + "\".");

		return nullptr;
	}


	// NOTE(Brendan): Just hanging on to this so we can later create the correct kind of node.
	Token token = tokens[index];


	// NOTE(Brendan): If we're currently on the "if" then we need to increment to look at the expression.
	index++;


	AstNode* expr = parse_expression(tokens, index);

	if (expr == nullptr) {

		return nullptr;
	}


	// NOTE(Brendan): Helm does not support single-line if statements. All if and else statements must use braces.
	if (tokens[index].kind != Token_OpenBrace) {

		parser_err(tokens[index], "Found token \"" + tokens[index].text + "\" where token \"{\" was expected. Unable to continue!");

		return nullptr;
	}

	// NODE(Brendan): This will create a new block. If we fail to create a new block, we error inside CreateNewBlock - no need to error here as well.
	// Just go ahead and return a nullptr;
	if (!CreateNewBlock(tokens[index])) {

		return nullptr;
	}


	// NOTE(Brendan): Just grabbing the correct node based on whether this is a #if or not.
	return (token.kind == Token_HashIf) ? create_node_hash_if(token, expr, currentBlock) : create_node__if(token, expr, currentBlock);
} // Ending "if" parsing.



// NOTE(Brendan): This function is a general purpose parsing function that can be run anytime we encounter the "for" keyword.
// Attempting to use this function to parse any other expression will result in an error and cause this function to return a nullptr.
// If the expression being parsed turns out to be a for loop that uses the "in" keyword, we simply call parse_for_in() and return its result.
AstNode* parse_for(std::vector<Token> tokens, u32 &index) {

	if (tokens[index].kind != Token_For) {

		parser_err(tokens[index], "Attempted to create a \"for\" loop with token \"" + tokens[index].text + "\".");

		return nullptr;
	}

	// NOTE(Brendan): Checking for the "in" keyword a little ahead of us, which this function wasn't made to handle.
	// Here's a visual example of why we add 4 to index:
	// for iter, value in list
	// ^   ^   ^ ^     ^
	// |   |   | |     |
	// |   |   | |     index + 4 ("in" keyword)
	// |   |   | |
	// |   |   | index + 3 (value)
	// |   |   |
	// |   |   index + 2 (comma)
	// |   |
	// |   index + 1 (iter)
	// |
	// index
	if (tokens[index + 4].kind == Token_In) {

		return parse_for_in(tokens, index);
	}

	// NOTE(Brendan): If the "in" keyword is not 4 places ahead of us, we may be skipping the "iter" declaration.
	// If so, here's a visual example of why we would add 2 to index:
	// for value in list
	// ^   ^     ^
	// |   |     |
	// |   |     index + 2 ("in" keyword)
	// |   |
	// |   index + 1 (value)
	// |
	// index
	else if (tokens[index + 2].kind == Token_In) {

		return parse_for_in(tokens, index);
	}


	// NOTE(Brendan): Storing the "for" keyword so we can use it at the end.
	Token keyword = tokens[index];

	bool assignmentComplete = false;

	AstNode* declaration = nullptr;
	AstNode* cond = nullptr;
	AstNode* step = nullptr;
	AstNode* body = nullptr;


	// NOTE(Brendan): Since we're looking at "for", increment index and move on.
	index++;

	bool inParen = tokens[index].kind == Token_OpenParen;


	if (inParen) {

		// NOTE(Brendan): If we're looking at a parenthesis, move forward.
		index++;
	}


	// NOTE(Brendan): Keep in mind that GetToken will increment index for us.
	declaration = GetType(tokens, index);


	if (declaration == nullptr) {

		parser_err(tokens[index - 1], "Unexpected token \"" + tokens[index - 1].text + "\" inside \"for\" expression. Unable to continue!");

		return nullptr;
	}


	if (tokens[index].kind == Token_Colon) {

		std::vector<AstNode*> dec;

		dec.push_back(declaration);

		dec = parse_var_creation(dec, tokens, index);


		if (dec.empty()) {

			parser_err(tokens[index - 1], "The \":\" operator is present inside of a for statement, though no variable name precedes it.");

			return nullptr;
		}

		if (dec.size() != 1) {

			parser_err(tokens[index - 1], "Unexpected token \"" + tokens[index - 1].text + "\" inside \"for\" expression. Unable to continue!");

			return nullptr;
		}


		// NOTE(Brendan): Before this, declaration was an identifier node. Now it's a creation node.
		declaration = dec[0];

		assignmentComplete = true;
	}


	// NOTE(Brendan): We aren't creating a new variable.
	else if (IsAssignment(tokens[index].kind)) {

		std::vector<AstNode*> dec;

		dec.push_back(declaration);

		// NOTE(Brendan): Before this, declaration was an identifier node. Now it's an assignment node.
		declaration = parse_var_assignment(dec, tokens, index);


		assignmentComplete = true;
	}


	// NOTE(Brendan): If we aren't creating or assigning a variable, then we're jumping straight to the conditional expression.
	if (!assignmentComplete) {

		// NOTE(Brendan): GetType incremented index. If there isn't actually a declaration or assignment happening at the beginning
		// however, then all it did is push us one step too far ahead. We need to back up index to the beginning of the expression.
		index--;

		declaration = nullptr;
	}

	// NOTE(Brendan): We completed the assignment segment.
	else if (tokens[index].kind != Token_Semicolon) {

		parser_err(tokens[index], "Expected token \";\" to follow declaration inside \"for\" statement. Token \"" + tokens[index].text + 
			"\" was found instead.");

		return nullptr;
	}

	// NOTE(Brendan): We completed the assignment segment and we _ARE_ looking at a semicolon.
	else {

		// NOTE(Brendan): Since we know we _ARE_ looking at a semicolon now, move index forward.
		index++;
	}


	// NOTE(Brendan): Declaration segment complete.
	// Beginning conditional segment.


	cond = parse_expression(tokens, index);

	if (cond == nullptr) {

		parser_err(keyword, "Encountered an error while parsing the conditional segment of the \"for\" statement. The error is likely somewhere"
		 " between tokens \"" + keyword.text + "\" " + keyword.pos.get_text_pos() + " and \"" + tokens[index].text + "\" " + 
		 tokens[index].pos.get_text_pos() + ".");

		return nullptr;
	} 

	// NOTE(Brendan): Conditional segment complete.
	// Beginning step segment.


	// NOTE(Brendan): It's time to parse the "step" segment of the "for" statement. (Usually incrementation or decrementation of a control variable.)
	// However, this segment can be skipped and that's perfectly fine, so we need to know we're _NOT_ skipping it before we try to parse it.


	// NOTE(Brendan): First we're checking to see if we're inside parenthesis, and if so, if the current token is a close parenthesis.
	// (In other words, if we're at the end of the "for" statement.) 
	// If not, we check to see if we're _NOT_ in parenthesis and if the current token is an open brace. (Again, this would just mean we're at the
	// end of the "for" statement.) 
	// If neither of these is true, then obviously the for statement hasn't ended and, as such, we need to parse the step segment.
	// If we _ARE_ at the end, then obviously we're skipping the step segment.
	if ((inParen && tokens[index].kind != Token_CloseParen) 
		|| (!inParen && tokens[index].kind != Token_OpenBrace)) {

		if (tokens[index].kind != Token_Semicolon) {

			parser_err(tokens[index], "Expected token \";\" to follow conditional segment of the \"for\" statement. Token \"" + tokens[index].text + 
				"\" was found instead.");

			return nullptr;
		}

		// NOTE(Brendan): Since we know we _ARE_ looking at a semicolon, move index forward.
		index++;

		step = parse_expression(tokens, index);

		if (step == nullptr) {

			parser_err(keyword, "Encountered an error while parsing the \"step\" segment of the \"for\" statement. The error is likely somewhere"
			 " between tokens \"" + keyword.text + "\" " + keyword.pos.get_text_pos() + " and \"" + tokens[index].text + "\" " + 
			 tokens[index].pos.get_text_pos() + ".");

			return nullptr;
		}
	}

	// NOTE(Brendan): Step segment ending.
	// Now checking the end of the statement to make sure it's valid.


	if (inParen) {

		if (tokens[index].kind != Token_CloseParen) {

			parser_err(tokens[index], "Missing closing parenthesis; either remove the opening parenthesis or add a closing parenthesis to the end.");

			return nullptr;
		}


		// NOTE(Brendan): If index _IS_ a close parenthesis.
		else {

			// NOTE(Brendan): Since we know index is a close parenthesis, increment it.
			index++;
		}
	} // NOTE(Brendan): Ending inParen check.


	// NOTE(Brendan): Helm does not support single-line else statements. All for statements must use braces.
	if (tokens[index].kind != Token_OpenBrace) {

		parser_err(tokens[index], "Found token \"" + tokens[index].text + "\" where token \"{\" was expected. Unable to continue!");

		return nullptr;
	}

	// NOTE(Brendan): If index _IS_ an open brace.
	else {


		// NODE(Brendan): This will create a new block. If we fail to create a new block, we error inside CreateNewBlock - no need to error here as well.
		// Just go ahead and return a nullptr;
		if (!CreateNewBlock(tokens[index])) {

			return nullptr;
		}

		body = currentBlock;
	}


	return create_node__for(keyword, declaration, cond, step, body);
} // NOTE(Brendan): Ending "for" parsing.



// NOTE(Brendan): This function is *only* to be used to parse "for" expressions with the "in" keyword.
// Attempting to use this function to parse a regular "for" loop (or any other expression) will result in an error and cause
// this function to return a nullptr.
AstNode* parse_for_in(std::vector<Token> tokens, u32 &index) {

	if (tokens[index].kind != Token_For) {

		parser_err(tokens[index], "Attempted to create a \"for\" loop with token \"" + tokens[index].text + "\".");

		return nullptr;
	}

	// NOTE(Brendan): Checking for the "in" keyword a little ahead of us. If it's not present, we're in the wrong function.
	// Here's a visual example of why we add 4 to index:
	// for iter, value in list
	// ^   ^   ^ ^     ^
	// |   |   | |     |
	// |   |   | |     index + 4 ("in" keyword)
	// |   |   | |
	// |   |   | index + 3 (value)
	// |   |   |
	// |   |   index + 2 (comma)
	// |   |
	// |   index + 1 (iter)
	// |
	// index 
	//
	//If the "in" keyword is not 4 places ahead of us, we may be skipping the "iter" declaration.
	// If so, here's a visual example of why we would add 2 to index:
	// for value in list
	// ^   ^     ^
	// |   |     |
	// |   |     index + 2 ("in" keyword)
	// |   |
	// |   index + 1 (value)
	// |
	// index
	if (tokens[index + 4].kind != Token_In && tokens[index + 2].kind != Token_In) {

		parser_err(tokens[index], "Expected token \"in\" in this statement. (Did you mean to parse a regular for statement instead?");

		return nullptr;
	}


	// NOTE(Brendan): Storing the "for" keyword so we can use it at the end.
	Token keyword = tokens[index];

	// NOTE(Brendan): This will be used later to store the "in" keyword.
	Token in;

	AstNode* iter = nullptr;
	AstNode* value = nullptr;
	AstNode* list = nullptr;
	AstNode* body = nullptr;


	// NOTE(Brendan): Since we're looking at "for", increment index and move on.
	index++;

	bool inParen = tokens[index].kind == Token_OpenParen;


	if (inParen) {

		// NOTE(Brendan): If we're looking at a parenthesis, move forward.
		index++;
	}


	// NOTE(Brendan): Keep in mind that GetToken will increment index for us.
	iter = GetType(tokens, index);


	if (iter == nullptr) {

		parser_err(tokens[index - 1], "Unexpected token \"" + tokens[index - 1].text + "\" inside \"for\" expression. Unable to continue!");

		return nullptr;
	}


	if (tokens[index].kind == Token_Comma) {

		// NOTE(Brendan): We're currently looking at a comma, so move forward.
		index++;

		// NOTE(Brendan): Once again, GetType increments index.
		value = GetType(tokens, index);

		// NOTE(Brendan): We had a comma but not a second value.
		if (value == nullptr) {

			parser_err(tokens[index - 1], "Unexpected declaration attempted of token \"" + tokens[index - 1].text + 
				"\" inside \"for\" expression. Unable to continue!");

			return nullptr;
		}
	}

	// NOTE(Brendan): If we're _NOT_ a comma.
	else {

		// NOTE(Brendan): It's perfectly ok to leave off the iter declaration. that just means that what we assigned iter to before
		// was actually our value variable.
		value = iter;
		iter = nullptr;
	}


	if (tokens[index].kind != Token_In) {

		parser_err(tokens[index], "Expected token \"in\" to follow declaration segment of the \"for\" statement. Token \"" + tokens[index].text + 
			"\" was found instead.");

		return nullptr;
	}


	// NOTE(Brendan): Grabbing and storing the location of the "in" keyword so we can use it at the end.
	in = tokens[index];

	// NOTE(Brendan): We're currently looking at the "in" keyword, so move forward.
	index++;


	if (tokens[index + 1].kind == Token_Ellipsis) {

		AstNode* left = GetType(tokens, index);

		if (left == nullptr) {

			parser_err(keyword, "Encountered an error while parsing the left side of the \"...\" keyword inside the \"for\" statement.");

			return nullptr;
		}


		list = parse_ellipsis(tokens, index);


		if (list == nullptr) {

			return nullptr;
		}


		list->ellipsis->left = left;
	}


	// NOTE(Brendan): If there's _NOT_ an ellipsis ahead of us.
	else if (IsType(tokens, index)) {

		list = GetType(tokens, index);
	}


	// NOTE(Brendan): If there's _NOT_ an ellipsis ahead of us _AND_ we're not a value.
	else {

		parser_err(tokens[index], "Unexpected token \"" + tokens[index].text + "\" following keyword \"in\". Unable to continue!");

		return nullptr;
	}


	if (list == nullptr) {

		parser_err(in, "Encountered an error while parsing the parameters for the \"in\" keyword inside the \"for\" statement.");

		return nullptr;
	}

	// NOTE(Brendan): List segment ending.
	// Now checking the end of the statement to make sure it's valid.


	if (inParen) {

		if (tokens[index].kind != Token_CloseParen) {

			parser_err(tokens[index], "Missing closing parenthesis; either remove the opening parenthesis or add a closing parenthesis to the end.");

			return nullptr;
		}


		// NOTE(Brendan): If index _IS_ a close parenthesis.
		else {

			// NOTE(Brendan): Since we know index is a close parenthesis, increment it.
			index++;
		}
	} // NOTE(Brendan): Ending inParen check.


	// NOTE(Brendan): Helm does not support single-line else statements. All for statements must use braces.
	if (tokens[index].kind != Token_OpenBrace) {

		parser_err(tokens[index], "Found token \"" + tokens[index].text + "\" where token \"{\" was expected. Unable to continue!");

		return nullptr;
	}

	// NOTE(Brendan): If index _IS_ an open brace.
	else {

		// NODE(Brendan): This will create a new block. If we fail to create a new block, we error inside CreateNewBlock - no need to error here as well.
		// Just go ahead and return a nullptr;
		if (!CreateNewBlock(tokens[index])) {

			return nullptr;
		}

		body = currentBlock;
	}


	return create_node_for_in(keyword, iter, value, list, body);
} // NOTE(Brendan): Ending "for in" parsing.



// NOTE(Brendan): This function parses, you guessed it, ellipsis. This is only to be used in the context of function calls and definitions.
AstNode* parse_ellipsis(std::vector<Token> tokens, u32 &index) {

	if (tokens[index].kind == Token_Ellipsis) {

		AstNode* type = nullptr;

		Token ellipsis = tokens[index];


		// NOTE(Brendan): There's no need to allow index to continue pointing to the ellipsis - move it forward.
		index++;

		type = GetType(tokens, index);


		if (type == nullptr) {

			parser_err(ellipsis, "Encountered an error while parsing the right side of the \"...\" keyword inside a function declaration.");

			return nullptr;
		}

		
		return create_node_ellipsis(ellipsis, nullptr, type);
	}

	// NOTE(Brendan): So if the current token is _NOT_ an ellipsis
	else {

		parser_err(tokens[index], "Attempted to create an ellipsis node with token \"" + tokens[index].text + "\": This is an issue with the parser.");

		return nullptr;
	}
} // NOTE(Brendan): Ending ellipsis parsing.


// NOTE(Brendan) This function handles everything after encountering a colon.
// If funcStyleParse is set to true, we expect a different sort of syntax to be around our variables. 
// (For example, it allows commas after declarations instead of just semicolons.)
// Returns a vector of any AstNodes created along the way.
std::vector<AstNode*> parse_var_creation(std::vector<AstNode*> floating_identifiers, std::vector<Token> tokens, u32 &index, bool funcStyleParse) {

	std::vector<AstNode*> results = std::vector<AstNode*>();

	Token token = tokens[index++];


	// NOTE(Brendan): If there are no identifiers to create variables with, obviously we can't proceed.
	if (floating_identifiers.empty()) {

		parser_err(token, "Attempted to create a new variable without providing a name.");

		return std::vector<AstNode*>();
	}


	//debug
	parser_wrn(floating_identifiers[0]->identifier->token, "Var creation: Creating a new variable named \"" + floating_identifiers[0]->identifier->token.text + "\".");


	// NOTE(Brendan): Typically, we don't want GetType to advance index for us; we want to do that ourselves, so we can do it carefully.
	// However, since we can wind up recursively calling this function while parsing function definitions, we have to leave the incrementation
	// of index up to parse_function_definition and the like.
	u32 indexPosition = index;

	AstNode* type = GetType(tokens, index);

	if (type == nullptr
		|| type->kind != NodeKind_FuncLiteral) {

		// NOTE(Brendan): Resets index to where it was before the call to GetType, unless we happen to be a function definition.
		index = indexPosition;
	}

	//debug
	parser_wrn(tokens[index], "Var creation: GetType complete - type is currently " + string(type != nullptr ? NODE_TYPE_NAME(type) : "null"));


	// NOTE(Brendan): This is checking for declarations with a type but no value.
	// It will usually be the case that we're looking at a regular variable declaration when this is the case.
	if (type != nullptr 
		&& tokens[index + 1].kind != Token_Colon
		&& !IsAssignment(tokens[index + 1].kind)) {


		// NOTE(Brendan): The above just verifies that we are only assigning a type and not a value. This verifies that we are
		// using proper syntax. If this is true, we should have at least one identifier followed by a colon, type, and a semicolon. 
		// Our code would look like:
		// var1, var2, var3, ... varN : type;
		// _HOWEVER!_ It is also possible we're declaring a function or function prototype, or the parameters inside of one.
		// That could look like the right half of the following expressions:
		// myFunc :: (var : type);
		// otherFunc := (var1, var2 : type1, var3 : type2)
		// anotherFunc :: ()
		// myThing :: myStruct {x, y : int, name : string};
		if (tokens[index + 1].kind == Token_Semicolon
			|| (funcStyleParse
				&& (tokens[index + 1].kind == Token_Comma
					|| tokens[index + 1].kind == Token_CloseParen
					|| tokens[index + 1].kind == Token_CloseBrace))) {


			//debug
			parser_wrn(tokens[index], "Var creation: #1 - Current token: " + tokens[index].text);


			results.push_back(create_node_assign_stmt(token, floating_identifiers, std::vector<AstNode*>(), type, true));


			// NOTE(Brendan): Everywhere else we increment index before returning. While it doesn't happen naturally here, by this point it's 
			// expected behavior. So we go ahead and increment index so that it matches everywhere else in this function.
			index++;

			return results;
		}

		// NOTE(Brendan): If we are *not* a valid type-only assignment.
		else {

			parser_err(token, "Invalid variable creation: Unexpected token \"" + tokens[index + 1].text + "\" encountered."
				" (You are likely missing a semicolon or a comma.)");

			return std::vector<AstNode*>();
		}
	}




	// NOTE(Brendan): If this is true, then we have one of four different possibilities:
	//
	// A) Both a type and another colon (a typed constant followed by a value).
	// Our code looks like:
	// var : type : 
	// Or, potentially:
	// var1, var2, ... varN : type : 
	//
	// B) A type and an assignment operator (a standard, typed variable).
	// Our code looks like:
	// var : type =
	// Or, potentially:
	// var1, var2, ... varN : type =  
	//
	// C) Two colons (a dynamically typed constant variable).
	// Our code looks like:
	// var ::
	// Or, potentially:
	// var1, var2, ... varN :: 
	//
	// Or D) No type, but an assignment operator (a standard, dynamically typed variable).
	// Our code looks like:
	// var := 
	// Or, potentially:
	// var1, var2, ... varN := 
	//
	// In any case, we must expect that a value (a literal, another variable, function call, etc...) will come next.
	else if ((type != nullptr 
		&& (tokens[index + 1].kind == Token_Colon
			|| tokens[index + 1].kind == Token_Equals))
		|| tokens[index].kind == Token_Colon
		|| tokens[index].kind == Token_Equals) {

		//debug
		parser_wrn(tokens[index], "Var creation: #2 A - Current token: " + tokens[index].text);


		// NOTE(Brendan): Looking for case A or C from the above description.
		bool isConst = tokens[index].kind == Token_Colon || (type != nullptr && tokens[index + 1].kind == Token_Colon);

		
		// NOTE(Brendan): In other words, case A or B above.
		if (type != nullptr 
			&& (tokens[index + 1].kind == Token_Colon 
				|| tokens[index + 1].kind == Token_Equals)) {

			// NOTE(Brendan): Index currently points to the type between the two colons. 
			// Increment it to point at the appropriate token so that we can use the parse_var_assignment function below.
			index++;
		}

		//debug
		parser_wrn(tokens[index], "Var creation: #2 B - Current token: " + tokens[index].text);


		AstNode* assignment = parse_var_assignment(floating_identifiers, tokens, index);


		if (assignment == nullptr) {

			parser_err(token, "Invalid variable creation: Could not parse value to be assigned.");

			return std::vector<AstNode*>();
		}


		// NOTE(Brendan): Always gotta do what error checking we can.
		// All assignment statements, expect for creating new functions and structs, must end with a semicolon.
		// Though we also must keep in mind that there may be more than one value, such as:
		// x, y := 6, 0;
		if (assignment->kind != NodeKind_Structure
			&& assignment->kind != NodeKind_FuncLiteral
			&& tokens[index].kind != Token_Semicolon
			&& (tokens[index].kind != Token_Comma
				&& !IsValue(tokens, index + 1))) {

				parser_err(tokens[index], "Expected a semicolon (\";\") to follow assignment statement started by token \"" + token.text + "\" " 
					+ token.pos.get_text_pos() + "; instead token \"" + tokens[index].text + "\" was found.");

				return std::vector<AstNode*>();
		}


		assignment->assign_stmt->type = type;
		assignment->assign_stmt->creation = true;
		assignment->assign_stmt->constant = isConst;


		results.push_back(assignment);

		return results;
	}


	// NOTE(Brendan): If we do not match any of the creation criteria above, then there's simply nothing left to do but error.
	else {

		parser_err(token, "Invalid variable declaration: the issue is likely between tokens \"" + token.text + "\" " +
			token.pos.get_text_pos() + " and \"" + tokens[index].text + "\" " + tokens[index].pos.get_text_pos() + ".");

		return std::vector<AstNode*>();
	}
}



// NOTE(Brendan): This function handles assignments.
// For example: 
// index = 7;
// index *= j;
// age, name = 9001, "Your mom";
AstNode* parse_var_assignment(std::vector<AstNode*> floating_identifiers, std::vector<Token> tokens, u32 &index) {


	std::vector<AstNode*> results;

	Token assignOp = tokens[index];

	TokenKind kind = assignOp.kind;


	if (!IsAssignment(kind) && kind != Token_Colon) {

		parser_err(assignOp, "Unexpected assignment triggered by token \"" + assignOp.text + "\".");

		return nullptr;
	}

	if (floating_identifiers.empty()) {

		parser_err(assignOp, "Encountered binary assignment operator \"" + assignOp.text + "\" without any named variables on the left side.");

		return nullptr;
	}


	// NOTE(Brendan): Now that we've checked our operator above, 
	// we can begin working with the first value of the expression on the right-hand side.
	index++;



	// NOTE(Brendan): For the most part, the right side of an assignment will be some kind of a value.
	// A struct is created and assigned in the same way as a value however, so let's go ahead and check for that first.
	if (tokens[index].kind == Token_Struct) {

		// NOTE(Brendan): If our structure has a body, we initialize it below.
		AstNode* result = create_node_structure(tokens[index], nullptr);


		index++;

		if (tokens[index].kind == Token_OpenBrace) {

			// NODE(Brendan): This will create a new block. If we fail to create a new block, we error inside CreateNewBlock - no need to error here as well.
			// Just go ahead and return a nullptr;
			if (!CreateNewBlock(tokens[index])) {

				return nullptr;
			}


			result->structure->body = currentBlock;

			// NOTE(Brendan): The two trues at the end are saying, in order, that we are a declaration and that we are a struct.
			std::vector<AstNode*> members = parse_args(tokens, index, true, true);


			// NOTE(Brendan): This should never happen on purpose, so if it does, error.
			if (members.empty()
				|| members.back() == nullptr) {

				parser_err(assignOp, "Invalid struct: Either there was an error parsing this struct\'s members list or this struct is empty.");

				return nullptr;
			}

			result->structure->body->block->stmts.insert(result->structure->body->block->stmts.end(), members.begin(), members.end());
		}

		return result;
	}


	// NOTE(Brendan): If the next token is *NOT* a value, function, or expression, we've got some problems.
	// The only exception would be if we had a value first and then an increment or decrement operator, like:
	// index++;
	// (Do note that kind currently points to the _FIRST_ token this function receives, not the _CURRENT_ token index points to.)
	if (!IsValue(tokens, index)
		&& tokens[index].kind != Token_OpenParen
		&& tokens[index].kind != Token_OpenBrace
		&& tokens[index].kind != Token_HashType
		&& kind != Token_Increment
		&& kind != Token_Decrement) {

		parser_err(tokens[index], "Unexpected token \"" + tokens[index].text + "\": A value to be assigned is required here.");

		return nullptr;
	}


	if (kind == Token_Increment || kind == Token_Decrement) {

		// NOTE(Brendan): If we have an expression where the increment or decrement operator came after the value it's incrementing or decrementing,
		// it has to be handled a little bit differently. (Since every other operator comes *before* the value.)
		// The index is currently looking at the token *after* the operator, so if this is the case, our code might look something like this:
		// var1++;
		// |   | |
		// |   | index (semicolon)
		// |   |
		// |   index - 1 (increment operator)
		// |
		// index - 2 (our value)
		if (IsValue(tokens, index - 2)) {

			// NOTE(Brendan): If we really are an increment/decrement operator that comes after our value, we need index to
			// be looking at the beginning of the statement.
			index -= 2;
		}

		// NOTE(Brendan): Otherwise, our code looks something like:
		// ++var1;
		// | |
		// | |
		// | |
		// | index (our value)
		// |
		// index - 1 (increment operator)
		else {

			// NOTE(Brendan): We need to be looking at the first part of the statement, so decrement index.
			index--;
		}
	}
	


	std::vector<AstNode*> values;

	bool continue_parsing;


	do {

		//debug
		parser_wrn(tokens[index], "Beginning assignment loop: current index is: " + tokens[index].text);


		Token token = tokens[index];

		kind = token.kind;

		continue_parsing = false;


		if (kind == Token_Semicolon) {

			break;
		}


		else if (kind == Token_EOF) {

			parser_err(assignOp, "Encountered EOF while parsing assignments!");

			return nullptr;
		}
	

		else if (kind == Token_HashRaw) {

			// NOTE(Brendan): If we find the #raw keyword but it is not followed by a "^", error.
			if (tokens[index + 1].kind != Token_Caret || !IsType(tokens, index + 2)) {

				parser_err(tokens[index], "Keyword \"" + tokens[index].text + "\" can only be used when immediately followed by a pointer.");

				return nullptr;
			}


			AstNode* pointer = GetType(tokens, index);

			if (pointer == nullptr) {

				parser_err(tokens[index], "Unable to parse the pointer after the token \"" + token.text + "\".");

				return nullptr;
			}


			values.push_back(create_node_hash_raw(token, pointer));
		}
	

		else if (kind == Token_HashType) {

			// NOTE(Brendan): If we find the #type keyword but it is not followed by a function definition, error.
			if (tokens[index + 1].kind != Token_OpenParen 
				|| !IsType(tokens, index + 2) 
				|| (tokens[index + 3].kind != Token_Comma 
					&& tokens[index + 3].kind != Token_Colon)) {

				parser_err(tokens[index], "Keyword \"" + tokens[index].text + "\" can only be used when immediately followed by a function prototype.");

				return nullptr;
			}

			// NOTE(Brendan): Moving index forward to get off of the "#type" keyword.
			index++;


			AstNode* func = GetType(tokens, index);

			if (func == nullptr) {

				parser_err(tokens[index], "Unable to parse the prototype after the token \"" + token.text + "\".");

				return nullptr;
			}


			values.push_back(create_node_hash_type(tokens[index - 1], func));
		}



		// NOTE(Brendan): If we wind up with a standalone comma in the middle of nowhere, we've got a few issues.
		if (kind == Token_Comma) {


			if (!IsValue(tokens, index - 1) && tokens[index - 1].kind != Token_CloseParen) {

				parser_err(tokens[index], "Unexpected token \",\": Encountered a comma that does not have a value before it.");

				return nullptr;
			}


			continue_parsing = true;


			// NOTE(Brendan): Since we have a comma and the loop doesn't increment index by itself, we need to advance it here.
			index++;

		} // NOTE(Brendan): Ending comma.



		// NOTE(Brendan): This condition checks for values that can be assigned to a variable.
		// (This includes values being incrmeented and decremented.)
		if (IsValue(tokens, index)
			|| kind == Token_Increment
			|| kind == Token_Decrement) {

			continue_parsing = true;

			values.push_back(parse_expression(tokens, index));


			// NOTE(Brendan): If we don't get a value or an expression here, something is definitely wrong.
			if (values.back() == nullptr) {

				parser_err(assignOp, "Encountered an error parsing the expression between tokens \"" + assignOp.text + "\" " +
					assignOp.pos.get_text_pos() + " and \"" + tokens[index].text + "\" " + tokens[index].pos.get_text_pos() + ".");

				return nullptr;
			}

		} // NOTE(Brendan): Ending IsValue.



		// Note(Brendan): If we have an open paranthesis, this will either be an expression, a function, or a function prototype. 
		// If we have an open brace, then we have an implicit array or struct.
		// In any event, GetType will handle it.
		if (kind == Token_OpenParen
			|| kind == Token_OpenBrace) {

			continue_parsing = true;


			AstNode* val = GetType(tokens, index);

			if (val == nullptr) {

				parser_err(token, "Encountered an error parsing assignment after encountering token \"" + token.text + "\" at " 
					+ token.pos.get_text_pos() + ".");

				return nullptr;
			}


			// NOTE(Brendan): You can't declare anything after a function anyway, so there's no need wasting any time. Just return here.
			if (val->kind == NodeKind_FuncLiteral) {
				
				//debug
				parser_wrn(assignOp, "Ending assignment: current index is: \"" + tokens[index].text + "\".");

				return val;
			}


			values.push_back(val);
		}


		// NOTE(Brendan): If we didn't get a value yet we set continue_parsing to true, something is definitely wrong.
		if (continue_parsing && values[values.size() - 1] == nullptr) {

			parser_err(assignOp, "Encountered an error parsing the expression between tokens \"" + assignOp.text + "\" " +
				assignOp.pos.get_text_pos() + " and \"" + tokens[index].text + "\" " + tokens[index].pos.get_text_pos() + ".");

			return nullptr;
		}

	} while (continue_parsing && tokens[index].kind != Token_EOF);


	//debug
	parser_wrn(assignOp, "Assignment ending: current index is: \"" + tokens[index].text + "\".");

	return create_node_assign_stmt(assignOp, floating_identifiers, values);
}


//NOTE(Brendan): Parses the arguments of a function and returns them as a vector of AstNodes. 
// This parses arguments both of a function being declared as well as a function being called.
// If this is a declaration, this function returns a vector of its arguments.
// If this is a function call, this returns a vector of the values being passed to it.
// If this is a struct definition, it returns a list of all of the members/methods defined within.
std::vector<AstNode*> parse_args(std::vector<Token> tokens, u32 &index, bool declaration, bool isStruct, bool isImplicitArray) {


	// NOTE(Brendan): We always want arguments to be inside of parenthesis, 
	// even if a function has no arguments the parenthesis are still required.
	// If index isn't pointing to an open parenthesis, error.
	if (tokens[index].kind != Token_OpenParen
		&& (!isStruct
			&& !isImplicitArray)) {

		parser_err(tokens[index], "Argument parsing expected token \"(\", not \"" + tokens[index].text + 
			"\". Unable to identify the beginning of the argument list.");

		return std::vector<AstNode*>();
	}


	// NOTE(Brendan): Very similar to the above. For implicit struct and array assignments, index must be pointing to
	// an open brace. If it isn't, error.
	if (tokens[index].kind != Token_OpenBrace
		&& (isStruct
			|| isImplicitArray)) {

		parser_err(tokens[index], "Struct member parsing expected token \"{\", not \"" + tokens[index].text + 
			"\". Unable to identify the beginning of the member list.");

		return std::vector<AstNode*>();
	}


	// NOTE(Brendan): Now that we know index points to an appropriate starting token, increment it.
	index++;



	std::vector<AstNode*> floating_identifiers = std::vector<AstNode*>();

	std::vector<AstNode*> results = std::vector<AstNode*>();

	TokenKind kind;

	// NOTE(Brendan): This helps us remember if we've encountered both a name and a type before. 
	// This is primarily used for keeping track of declarations: non-declarations will never have types.
	// Here's an example of a declaration with both a variable name and a type:
	// (var1 : int)
	// This is opposed to having only types, such as:
	// (int, bool, double)
	// This is also useful for implicit assignments of arrays and structs.
	bool typesOnly = declaration || isStruct || isImplicitArray;


	for (Token token; tokens[index].kind != Token_CloseParen;) {

		token = tokens[index];

		kind = token.kind;

		//debug
		parser_wrn(token, "Argument loop: index is: " + token.text);


		if (kind == Token_Comma) {


			// NOTE(Brendan): In every other possibility we either call a function that increments index or we return an error. Since we do neither
			// here, we must manually increment index ourselves.
			index++;

			continue;
		}


		else if (kind == Token_EOF) { 

			parser_err(token, "Encountered EOF while parsing " + string(isStruct ? "struct members" : "function arguments") + ".");

			return std::vector<AstNode*>();
		}


		else if (kind == Token_Semicolon) {

			parser_err(token, "Unexpected \";\" encountered. The line has been ended before all " + string(isStruct ? "members" : "arguments") + " could be parsed!");

			return std::vector<AstNode*>();
		}


		else if (kind == Token_CloseBrace) {

			if (isStruct
				|| isImplicitArray) {

				break;
			}

			else {

				parser_err(token, "Unexpected \"}\" encountered. The line has been ended before all arguments could be parsed!");

				return std::vector<AstNode*>();
			}
		}


		else if (kind == Token_OpenParen) {


			if (declaration) {

				parser_err(token, "Unexpected token \"(\": Unable to parse " + string(isStruct ? "members" : "parameters") + ".");

				return std::vector<AstNode*>();
			}

			// NOTE(Brendan): If _NOT_ a declaration. (i.e. a function call.)
			else {

				//debug
				parser_wrn(token, "New argument is a parenthetical expression: \"" + token.text + "\".");


				// NOTE(Brendan): Keep in mind that parse_expression *will* increment index for us.
				results.push_back(parse_expression(tokens, index));


				if (results.back() == nullptr) {

					parser_err(tokens[index], "Unable to parse expression: Cannot continue parsing " + string(isStruct ? "members" : "arguments") + ".");

					return std::vector<AstNode*>();
				}
			}
		}



		else if (IsValue(tokens, index)) {


			if (declaration) {

				if (IsLiteral(kind)) {

					parser_err(token, "Unexpected token \"" + token.text + "\"! Cannot declare a " + (isStruct ? "struct" : "function") + 
						" with a literal " + (isStruct ? "member" : "parameter") + ".");

					return std::vector<AstNode*>();
				}


				// NOTE(Brendan) If _NOT_ a literal.
				else {

					// NOTE(Brendan): GetType will increment index for us.
					AstNode* identifier = GetType(tokens, index);

					if (identifier == nullptr) {

						parser_err(token, "Encountered an error parsing arguments after encountering token \"" + token.text + "\" at " 
							+ token.pos.get_text_pos() + ".");

						return std::vector<AstNode*>();
					}


					floating_identifiers.push_back(identifier);
				}
			}

			// NOTE(Brendan): If this is _NOT_ a declaration. (i.e. a function call, implicit struct, etc.)
			else {

				// NOTE(Brendan): We're checking here for implicit struct definitions where a member value is explicitly assigned.
				// This would look something like this:
				// var = myClass {4 + 7, otherVar = 6, name = "Steve"}
				if (isStruct) {

					//debug
					u32 tempIndex = index;


					//debug
					parser_wrn(tokens[index], "About to call parse_expression from parse_args->IsValue->not declaration->isStruct");


					// NOTE(Brendan): GetType will increment index for us.

					//debug
					//AstNode* var = parse_expression(tokens, tempIndex);
					AstNode* var = parse_expression(tokens, index);


					if (var == nullptr) {

						parser_err(token, "Encountered an error parsing struct members after encountering token \"" + token.text + "\".");

						return std::vector<AstNode*>();
					}


					//debug
					parser_wrn(tokens[index], "We should definitely be here!");

					// NOTE(Brendan): Now we check to see if we are a variable being assigned to anything.
					//debug
					//if (tokens[tempIndex].kind == Token_Equals) {
					if (tokens[index].kind == Token_Equals) {

						std::vector<AstNode*> floating_identifiers = std::vector<AstNode*>();


						floating_identifiers.push_back(var);


						// NOTE(Brendan): parse_var_assignment will also increment index for us.
						//debug
						//AstNode* assignment = parse_var_assignment(floating_identifiers, tokens, tempIndex);
						AstNode* assignment = parse_var_assignment(floating_identifiers, tokens, index);


						if (assignment == nullptr) {

							parser_err(tokens[index], "Unable to parse value to be assigned inside of member definition.");

							return std::vector<AstNode*>();
						}


						results.push_back(assignment);

						typesOnly = false;


						index = tempIndex;
					}


					// NOTE(Brendan): If we are *not* assigning a value to a named variable, but we have before.
					else if (!typesOnly) {

						parser_err(tokens[index], "Invalid implicit struct declaration: You cannot attempt an implicit (unnamed) member assignment after an "
							"explicit (named) assignment. (For example, {6, var = 7}; is valid, but {var = 7, 6}; is not.)");

						return std::vector<AstNode*>();
					}


					// NOTE(Brendan): If we are *not* assigning a value to a named variable, and we _ARE_ typeOnly.
					else {

						// NOTE(Brendan): Index is incremented inside of parse_expression.
						results.push_back(var);
					}
				}


				// NOTE(Brendan): If we are _NOT_ a struct.
				else {

					//debug
					parser_wrn(tokens[index], "About to call parse_expression from parse_args->IsValue->not declaration->not struct");

					// NOTE(Brendan): Once again, keep in mind that index is incremented inside of parse_expression.
					results.push_back(parse_expression(tokens, index));


					if (results.back() == nullptr) {

						parser_err(tokens[index], "Unable to parse function call.");

						return std::vector<AstNode*>();
					}
				}
			}
		} // NOTE(Brendan): Ending IsValue check.



		else if (tokens[index].kind == Token_Ellipsis) {

			if (!floating_identifiers.empty()) {

				parser_err(tokens[index], "Variable(s) are present to the left of unary ellipsis operator, "
					"which can be used only with a single variable to its right. Perhaps a comma is missing?");

				return std::vector<AstNode*>();
			}


			// NOTE(Brendan): parse_ellipsis will, just as most functions, increment index on its own.
			AstNode* ellipsis = parse_ellipsis(tokens, index);

			if (ellipsis == nullptr) {

				parser_err(tokens[index], "There was an error parsing the ellipsis inside a " + string(isStruct ? "struct" : "function") + " definition.");

				return std::vector<AstNode*>();
			}
			
			results.push_back(ellipsis);
		} // NOTE(Brendan): Ending ellipsis argument parsing.



		// NOTE(Brendan): Creating a new variable.
		else if (kind == Token_Colon) {


			if (floating_identifiers.empty()) {

				parser_err(tokens[index], "Invalid variable declaration: variable name was not provided. (Is there an extra comma?)");

				return std::vector<AstNode*>();
			}


			// NOTE(Brendan): Just grabbing this to use later.
			Token colon = tokens[index];

			std::vector<AstNode*> args = std::vector<AstNode*>();

			// NOTE(Brendan): Since encountering a colon means we are declaring the type of a variable, we can
			// infer that we *must* have encountered something other than a type already.
			typesOnly = false;



			if (tokens[index + 1].kind == Token_Ellipsis) {

				// NOTE(Brendan): Increment index to point to the ellipsis.
				index++;

				if (floating_identifiers.size() > 1) {

					parser_err(tokens[index], "Only one ellipsis typed variable can be declared at a time. You cannot declare multiple variables here.");

					return std::vector<AstNode*>();
				}


				// NOTE(Brendan): Once again, parse_ellipsis will increment index on its own.
				AstNode* ellipsis = parse_ellipsis(tokens, index);

				if (ellipsis == nullptr) {

					parser_err(tokens[index], "There was an error parsing the ellipsis inside a " + string(isStruct ? "struct" : "function") + " definition.");

					return std::vector<AstNode*>();
				}

				
				args.push_back(create_node_assign_stmt(colon, floating_identifiers, std::vector<AstNode*>(), ellipsis, true));
			} // NOTE(Brendan): Ending ellipsis parameter creation.


			// NOTE(Brendan): This will be the code run the majority of the time.
			// The only case where this isn't the path chosen is if we're defining a variable with an ellipsis type. 
			// (And only then because it's almost always invalid outside of function definitions.)
			else {

				// NOTE(Brendan): The true on the end enables a different (generally more lenient) style of 
				// variable parsing specific to function and struct definitions.
				// It's also worth remembering that this function *will* increment index for us.
				args = parse_var_creation(floating_identifiers, tokens, index, true);
			}

			floating_identifiers.clear();


			// NOTE(Brendan): A bit of error checking.
			if (args.empty()) {

				if (isStruct) {

					parser_err(token, "Encountered an error while parsing the members in a struct definition. The error is likely somewhere"
					 " in the parser or lexer rather than the expression itself.");
				}

				// NOTE(Brendan): If _NOT_ a struct. (i.e. a function definition, prototype or call.)
				else {

					parser_err(token, "Encountered an error while parsing the arguments in a function definition or prototype. The error is likely somewhere"
					 " in the parser or lexer rather than the expression itself.");
				}

				return std::vector<AstNode*>();
			}

			results.insert(results.end(), args.begin(), args.end());
		} // NOTE(Brendan): Ending colon (parameter creation) parsing.
	}


	// NOTE(Brendan): Some basic sanity checking.
	if (!isStruct
		&& !isImplicitArray) {

		if (tokens[index].kind == Token_CloseParen) {

			// NOTE(Brendan): The only way for us to wind up here is if we parsed the arguments without failing and encountered a close parenthesis at the end.
			// In other words, index must be pointing to a close parenthesis right now. Go ahead and increment it.
			index++;
		}

		// NOTE(Brendan): We are a function and the current index is *not* a close parenthesis.
		else {

			parser_err(tokens[index], "Failed to parse all arguments: The argument parsing loop has exited on unexpected token \"" 
				+ tokens[index].text + "\". The error is likely somewhere in the parser or lexer rather than the expression itself.");

			return std::vector<AstNode*>();
		}
	}


	// NOTE(Brendan): If we _ARE_ an implicit struct or array.
	else if (tokens[index].kind != Token_CloseBrace) {

		parser_err(tokens[index], "Failed to parse all " + (string)(isStruct ? "struct members" : "array elements") + 
		": The parsing loop has exited unexpectedly on token \"" + tokens[index].text + "\". The error is likely somewhere in"
		" the parser or lexer rather than the expression itself.");

		return std::vector<AstNode*>();
	}



	// NOTE(Brendan): Checking for tokens that were never used to create a variable. (So long as we aren't *just* types, anyway.)
	if (!floating_identifiers.empty()
			&& !typesOnly) {

		string names = "";

		for (AstNode* node : floating_identifiers) {

			names += "\"" + node->identifier->token.text + "\"\n";
		}

		parser_err(tokens[index], "Encountered the end of the parameter list before all variables parsed were assigned a type. "
			"Variable(s) without a type are:\n" + names);

		return std::vector<AstNode*>();
	}


	// NOTE(Brendan): In the event that we only had types, then results (which ordinarily holds the newly defined variables)
	// will be empty. Instead what we will want is floating_identifiers. We *could* go through all of the effort of
	// pushing all of the nodes from floating_identifiers into results, but... 
	// it's a lot easier to just return floating_identifiers directly.
	else if (typesOnly
			&& !isImplicitArray) {
				
		return floating_identifiers;
	}

	//debug
	parser_wrn(tokens[index], "Argument parsing complete. Index is: " + tokens[index].text);

	return results;
}


// NOTE(Brendan): A utility function made for the purpose of cleaning GetType up a bit.
// It returns a FuncLiteral AstNode and assigns all of the appropriate information, including creating a new block if necessary.
AstNode* parse_function_definition(std::vector<Token> tokens, u32 &index) {

	Token funcToken = tokens[index - 1];

	std::vector<AstNode*> returnTypes = std::vector<AstNode*>();


	AstNode* name = nullptr;
	AstNode* lib = nullptr;

	AstNode* body = nullptr;


	std::vector<AstNode*> args = parse_args(tokens, index, true);

	if (!args.empty()
		&& args.back() == nullptr) {

		parser_err(tokens[index], "Encountered an error parsing the parameters of a function.");

			return nullptr;
	}


	Token token = tokens[index];


	//debug
	parser_wrn(token, "Back in function definition! Current token: " + token.text);


	// NOTE(Brendan): Figuring out our return type(s). (Token_Arrow just means the "->" operator.)
	if (token.kind == Token_Arrow) {


		// NOTE(Brendan): Since we already know index is pointing to the arrow, go ahead and move it forward.
		index++;


		// NOTE(Brendan): It's perfectly valid to put your return types inside of a set of parenthesis, though it doesn't really achieve much.
		// Here's an example:
		// getPosition :: () -> (int, int) 
		if (tokens[index].kind == Token_OpenParen) {

			// NOTE(Brendan): If we already know that the next index is an open parenthesis, then we can skip it.
			index++;
		}


		// NOTE(Brendan): We just loop until we hit either a semicolon, open brace, EOF, or conditionally a closing parenthesis.
		do {

			// NOTE(Brendan): There are a few error checks below, then we make a call to GetType.
			// This call to GetType is what increments the value of index in this loop!
			
			//debug
			parser_wrn(tokens[index], "Currently inside return type definition loop! Current token: " + tokens[index].text);



			if (tokens[index].kind == Token_EOF) {

				parser_err(token, "Encountered EOF while parsing function return types.");

				return nullptr;
			}

			if (tokens[index].kind == Token_OpenBrace) {

				break;
			}



			if (!returnTypes.empty() 
				&& returnTypes.back() == nullptr) {

				parser_err(token, "Encountered an error parsing the return types of a function between tokens \"" + token.text + "\" " +
					token.pos.get_text_pos() + " and \"" + tokens[index].text + "\" " + tokens[index].pos.get_text_pos() + ".");

				return nullptr;
			}



			if (tokens[index].kind == Token_HashForeign) {

				// NOTE(Brendan): Since we don't really care about the HashForeign token, we move forward again.
				index++;


				if (tokens[index].kind == Token_String) {

					name = GetType(tokens, index);

					if (name == nullptr) {

						parser_err(tokens[index], "Encountered an error parsing the local name for a \"#foreign\" keyword.");

						return nullptr;
					}
				}


				if (IsType(tokens, index)) {

					lib = GetType(tokens, index);
				}


				// NOTE(Brendan): If there simply was no lib, or it wound up being a nullptr.
				if (lib == nullptr) {

					parser_err(tokens[index], "Failed parsing \"#foreign\" keyword: While the link name is optional, the library parameter is required.");

					return nullptr;
				}

			
				//debug
				parser_wrn(tokens[index], "Done parsing #foreign -  Current token: " + tokens[index].text);

			} // NOTE(Brendan): Ending #foreign check.



			// NOTE(Brendan): Just a friendly reminder that GetType will increment index.
			// The first false parameter explicitly states that we *DO* want the function to move index for us. 
			// (This is by default set to false, and is typically unnecessary; however we need to send another parameter after it so it must be included.)
			// The second boolean, true, states that we _DO NOT_ want to parse implicit structs and arrays. (Being that we're at the end of a declaration already,
			// allowing this behavior would result in the parser believing that our last parameter was, itself, an implicit struct and... bad things happen.)
			AstNode* reType = GetType(tokens, index, false, true);

			if (reType == nullptr) {

				parser_err(tokens[index], "Failed parsing return types.");

				return nullptr;
			}


			returnTypes.push_back(reType);



			if (tokens[index].kind == Token_Comma) {

				// NOTE(Brendan): Since we already know what index is, increment it.
				index++;

				continue;
			}


		} while (tokens[index].kind != Token_OpenBrace && tokens[index].kind != Token_Semicolon && tokens[index].kind != Token_CloseParen);
	} // NOTE(Brendan): End return type parsing.



	//debug
	parser_wrn(tokens[index], "Parsing return types complete!");



	// NOTE(Brendan): Creating a new block for the function body.
	if (tokens[index].kind == Token_OpenBrace) {

		// NODE(Brendan): This will create a new block. If we fail to create a new block, we error inside CreateNewBlock - no need to error here as well.
		// Just go ahead and return a nullptr;
		if (!CreateNewBlock(tokens[index])) {

			return nullptr;
		}

		body = currentBlock;

		// NOTE(Brendan): SInce we know index points to an open brace, go ahead and increment.
		index++;
	}


	// NOTE(Brendan): If index isn't pointing to an open brace and the loop above exits because of a close parenthesis, we may looking at a line like:
	// getPath :: (getPos : (id : int) -> vec3) -> (path) {
	//                                                  ^
	//                                                  |
	//                                                  index (close parenthesis)
	// In a situation like this, index will be pointing to the last close parenthesis rather than the open brace directly after it.
	// Let's check the next index to see if it's an open brace.
	else if (tokens[index].kind == Token_CloseParen
		&& tokens[index + 1].kind == Token_OpenBrace) {


		// NOTE(Brendan): Since we know we're currently looking at a close parenthesis, and the next token is an open brace,
		// advance index forward so we can create a new block.
		index++;

		// NODE(Brendan): This will create a new block. If we fail to create a new block, we error inside CreateNewBlock - no need to error here as well.
		// Just go ahead and return a nullptr;
		if (!CreateNewBlock(tokens[index])) {

			return nullptr;
		}


		body = currentBlock;

		// NOTE(Brendan): SInce we know index points to an open brace, go ahead and increment.
		index++;
	}


	// NOTE(Brendan): If we don't begin a function body _AND_ we don't end the line, the last ditch effort is to consider whether we're part of a line like:
	// getPath :: (getPos : (id : int) -> vec3) -> path
	//                                        ^
	//                                        |
	//                                        End of the "getPos" function declaration - where we could be now!
	else if(tokens[index].kind != Token_Semicolon 
		&& tokens[index].kind != Token_OpenBrace
		&& tokens[index].kind != Token_CloseParen) {

		parser_err(token, "Failed parsing function declaration: either the line must end with a semicolon or there must be a function body.");

		return nullptr;
	}



	// NOTE(Brendan): This is just checking to see if args has a name and a type, like:
	// (someVar : int)
	if (!args.empty()
		&& args.front()->kind == NodeKind_AssignStmt) {

		return create_node_func_literal(funcToken, args, std::vector<AstNode*>(), returnTypes, body, name, lib);
	}

	// NOTE(Brendan): If we're here, we *only* have a type. For example:
	// (int, bool)
	else {

		return create_node_func_literal(funcToken, std::vector<AstNode*>(), args, returnTypes, body, name, lib);
	}
} // NOTE(Brendan): Ending parse_function_definition.


// NOTE(Brendan): Creates and returns an AstNode for the kind passed, so long as it is a type. (i.e. an int, string, etc... or a variable.)
// The flag indexReturn, if true, resets the index at the end of the function back to where it was when GetType was first called.
// ignoreImplicit does exactly what it sounds like: If true, it ignores open braces, so it will not attempted to create implicit structs or arrays.
// Bother of these flags are set to false by default!
AstNode* GetType(std::vector<Token> tokens, u32 &index, bool indexReturn, bool ignoreImplicit) {

	// NOTE(Brendan): This is used to store the position of index before it's modified by this function.
	// If indexReturn is set to true, we will set index back to this value before ending the function.
	u32 entryPosition = index;


	//debug
	parser_wrn(tokens[index], "Getting type of token: " + tokens[index].text);


	Token token = tokens[index++];

	AstNode* result = nullptr;


	if (IsLiteral(token.kind)) {

		result = create_node_literal(token);
	}


	else if (token.kind == Token_Increment || token.kind == Token_Decrement) {

		if (!IsType(tokens, index)) {

			parser_err(tokens[index], "Unexpected token \"" + tokens[index].text + "\": A value to be " + 
				(token.kind == Token_Increment ? "incremented" : "decremented") + " is required here.");

			return nullptr;
		}


		AstNode* type = GetType(tokens, index);

		if (type == nullptr) {

			parser_err(tokens[index], "Unable to create " + string(token.kind == Token_Increment ? "incrementation" : "decrementation") + " node: "
				"Unable to determine type of node to be " + string(token.kind == Token_Increment ? "incremented" : "decremented") + ".");

			return nullptr;
		}


		result = create_node_incr_decr_stmt(token, type, true);

		// NOTE(Brendan): Since we know we're looking at a value, move index forward.
		index++;

	} // NOTE(Brendan): Ending increment and decrement unary operators.


	// NOTE(Brendan): If we start with an open parenthesis, we're dealing with an expression or a function declaration.
	// This can get a little complicated, so brace yourselves.
	else if (token.kind == Token_OpenParen) {


		// NOTE(Brendan): This is checking to see if we're a function or function prototype.
		// First we check to see if there is a close parenthesis immediately, i.e. "()"
		// If not, then we need to see if the open parenthesis is followed by a type.
		// Then, we check to see if the character after that is either a comma, colon, or a close parenthesis.
		if (tokens[index].kind == Token_CloseParen
			|| (IsType(tokens, index) 
			&&    (tokens[index + 1].kind == Token_Comma 
				|| tokens[index + 1].kind == Token_CloseParen
				|| tokens[index + 1].kind == Token_Colon))) {


			// NOTE(Brendan): Checking to see if we're defining a new function or function prototype. Some visual aid:
			// name :: (firstParam
			//       ^ ^^
			//       | || 
			//       | |index (first parameter)
			//       | | 
			//       | index - 1 (open parenthesis)
			//       | 
			//       index - 2 (colon - What we need!)
			//
			// NOTE(Brendan): There's also the possibility that we could have the "#type" keyword in front:
			// name :: #type (firstParam
			//         ^     ^^
			//         |     || 
			//         |     |index (first parameter)
			//         |     | 
			//         |     index - 1 (open parenthesis)
			//         |
			//         index - 2 (#type - also ok!)
			if (tokens[index - 2].kind == Token_Colon
				|| tokens[index - 2].kind == Token_HashType) {


				// NOTE(Brendan): To call parse_function_definition, we need to be looking at the open parenthesis.
				index--;

				result = parse_function_definition(tokens, index);
			}

			// NOTE(Brendan): If we're just some random function prototype that's _NOT_ being defined, error.
			else {

				parser_err(token, "Unexpected function prototype.");

				return nullptr;
			}
		}


		// NOTE(Brendan): If we are _NOT_ a function prototype. (i.e. an expression.)
		else {

			// NOTE(Brendan): To properly parse the expression, we need to be looking at the open parenthesis.
			index--;

			result = parse_expression(tokens, index);


			if (result == nullptr) {

				parser_err(tokens[index], "Could not parse expression.");

				return nullptr;
			}
		}

	} // NOTE(Brendan): End open parenthesis parsing.


	else if (token.kind == Token_Identifier) {


		// NOTE(Brendan): This is an accessor. For example:
		// my_class.my_method();
		// var : int = my_class.my_member;
		if (tokens[index].kind == Token_Period) {


			// NOTE(Brendan): We'll need this to create our accessor node later.
			Token period = tokens[index];

			// NOTE(Brendan): Since we know index is a period, we can safely increment it.
			index++;


			AstNode* type = GetType(tokens, index);

			if (type == nullptr) {

				parser_err(token, "Encountered an error parsing the expression between tokens \"" + token.text + "\" " +
					token.pos.get_text_pos() + " and \"" + tokens[index].text + "\" " + tokens[index].pos.get_text_pos() + ".");

				return nullptr;
			}


			// NOTE(Brendan) Thanks to type recursively calling GetType, this winds up forming a structure of nested AstNodes where
			// my_lib.my_class.my_func();
			// becomes:
			// Accessor{my_lib, Accessor{My_class, FuncCall{my_func}}}
			result = create_node_accessor(period, create_node_identifier(token), type);
		}


		else if (tokens[index].kind == Token_OpenBracket) {

			// NOTE(Brendan): We're going to hang on to this so we can use it later.
			Token arrayToken = tokens[index];

			// NOTE(Brendan): Since we know index is an open bracket, we can safely increment it.
			index++;


			if (!IsValue(tokens, index)) {

				parser_err(arrayToken, "A value is required after token \"[\"; Token \"" + tokens[index].text + "\" was found instead.");

				return nullptr;
			}


			result = parse_expression(tokens, index);

			if (result == nullptr) {

				parser_err(tokens[index], "Unable to parse array index expression. Is the expression valid? "
					"(Check for trailing arithmetic operators and unpaired parenthesis.)");

				return nullptr;
			}



			result = create_node_array(arrayToken, create_node_identifier(token), result);
		}


		// NOTE(Brendan): An identifier followed by an open parenthesis has to be a function.
		else if (tokens[index].kind == Token_OpenParen) {

			std::vector<AstNode*> args = std::vector<AstNode*>();

			args = parse_args(tokens, index);

			if (!args.empty()
				&& args.back() == nullptr) {

				parser_err(tokens[index], "Unable to parse function arguments. Is the expression valid? "
					"(Check for trailing arithmetic operators and unpaired parenthesis.)");

				return nullptr;
			}


			result = create_node_call_expr(token, create_node_identifier(token), args);
		}


		else if (tokens[index].kind == Token_Increment || tokens[index].kind == Token_Decrement) {

			result = create_node_incr_decr_stmt(tokens[index], create_node_identifier(token), false);


			// NOTE(Brendan): Since we know we're looking at an increment or decrement operator, move index forward.
			index++;
		}


		// NOTE(Brendan): An assignment to an implicit struct would look like this:
		// var = MyStruct {"Hello", 27 + 6, -16, true, otherVar};
		else if (!ignoreImplicit
			&& tokens[index].kind == Token_OpenBrace) {


			//debug
			parser_wrn(tokens[index], "Found an implicit struct!");


			// NOTE(Brendan): The false second before the end is saying that we *are not* creating a new struct (i.e. we are not expecting names and types, 
			// but rather values (or perhaps even names and values)). The true at the end is saying that we *are* parsing a struct, not a function.
			std::vector<AstNode*> implicitStructData = parse_args(tokens, index, false, true);

			// NOTE(Brendan): Performing a bit of error checking to ensure we parsed the struct ok.
			if (!implicitStructData.empty()
				&& implicitStructData.back() == nullptr) {

				parser_err(tokens[index], "Unable to parse implicit struct member values. Is the expression valid? "
					"(Check for trailing arithmetic operators, unpaired parenthesis and the like.)");

				return nullptr;
			}


			if (tokens[index].kind != Token_CloseBrace) {

				parser_err(tokens[index], "Parser was unable to find a closing brace (\"}\") at the end of an implicit struct creation statement."
					" Make sure your statement has a closing brace immediately before the semicolon!");

				return nullptr;
			}


			// NOTE(Brendan): Since we know we are pointing to a close brace (and the matching opening brace would have told us we were
			// an implicit struct, and thus did not open a new block) we can go ahead and increment index here without worry.
			index++;


			result = create_node_implicit_struct(token, implicitStructData);
		}

		// NOTE(Brendan): If we are not an accessor, function, increment/decrement statement, nor an implicit struct, we're just a normal variable or type.
		else {

			result = create_node_identifier(token);
		}
	} // NOTE(Brendan): End identifier parsing.



	// NOTE(Brendan): You would assign a variable to an implicit array like:
	// var = {1, otherVar, -16, 4 + 3};
	else if (!ignoreImplicit
		&& token.kind == Token_OpenBrace) {

		// NOTE(Brendan): We always assign token to the current index, then increment index.
		// In this case, however, we actually *want* the open brace, not what comes after it.
		index--;


		// NOTE(Brendan): The false, false, and true parameters at the end are saying 
		// 1) we are not creating a new function/struct, 
		// 2) we are not using struct-style (i.e. braces not parenthesis) definitions, and
		// 3) we *are* an implicit array.
		std::vector<AstNode*> implicitArray = parse_args(tokens, index, false, false, true);

		if (!implicitArray.empty()
			&& implicitArray.back() == nullptr) {

			parser_err(tokens[index], "Unable to parse implicit array values. Is the expression valid? "
				"(Check for trailing arithmetic operators, unpaired parenthesis and the like.)");

			return nullptr;
		}


		if (tokens[index].kind != Token_CloseBrace) {

			parser_err(tokens[index], "Parser was unable to find a closing brace (\"}\") at the end of an implicit array creation statement."
				" Make sure your statement has a closing brace immediately before the semicolon!");

			return nullptr;
		}


		// NOTE(Brendan): Since we know we are pointing to a close brace (and the matching opening brace would have told us we were
		// an implicit array, and thus did not open a new block) we can go ahead and increment index here without worry.
		index++;


		result = create_node_implicit_array(token, implicitArray);
	}


	if (indexReturn) {

		index = entryPosition;
	}


	//debug
	parser_wrn(tokens[index], "GetType returning with token \"" + tokens[index].text + "\"");


	return result;

} // NOTE(Brendan): Ending GetType.



// NOTE(Brendan): Makes sure the kind passed is a literal. (i.e. 16, "Hello, World", -12.3339, etc...)
force_inline_all bool IsLiteral(TokenKind kind) {

	return (kind > Token__LiteralBegin && kind < Token__LiteralEnd) && kind != Token_Identifier;
}

// NOTE(Brendan): Makes sure the kind passed is an arithmetic operator. (i.e. +, -, ^, etc...)
force_inline_all bool IsArithmetic(TokenKind kind) {

	return kind > Token__ArithmeticBegin && kind < Token__ArithmeticEnd;
}

// NOTE(Brendan): Makes sure the kind passed is an assignment operator. (i.e. +, -, ^, etc...)
force_inline_all bool IsAssignment(TokenKind kind) {

	return (kind > Token__AssignOpBegin && kind < Token__AssignOpEnd) || kind == Token_Equals;
}

// NOTE(Brendan): Makes sure the kind passed is a comparison operator. (i.e. >, <, ==, etc...)
force_inline_all bool IsComparison(TokenKind kind) {

	return (kind > Token__ComparisonBegin && kind < Token__ComparisonEnd);
}


// NOTE(Brendan): Makes sure the kind passed is a type. (i.e. something a variable could be created with: int, string, class, etc...)
force_inline_all bool IsType(std::vector<Token> tokens, u32 index) {


	if (tokens[index].kind == Token_Identifier) {

		return true;
	}

	// NOTE(Brendan): In other words, this just says "hey, a pointer can be a type too!" like in this example:
	// var : ^my_class = 0;
	if (tokens[index].kind == Token_Caret 
		&& tokens[index + 1].kind == Token_Identifier) {

		return true;
	}

	// NOTE(Brendan): The #raw keyword simply changes how a pointer is created and behaves - it should still definitely count as a value.
	// Here's an example of how it could work:
	// var : #raw ^my_class = 0;
	if (tokens[index].kind == Token_HashRaw 
		&& tokens[index + 1].kind == Token_Caret 
		&& tokens[index + 2].kind == Token_Identifier) {

		return true;
	}


	return false;
}

// NOTE(Brendan): Makes sure the kind passed is a value. (i.e. either a variable, value returned from a function, a literal, etc...)
force_inline_all bool IsValue(std::vector<Token> tokens, u32 index) {


	// NOTE(Brendan): In case we're looking at a negative (like -6) we want to return true, 
	// rather than seeing a subtraction sign and being like "Nope, no value here!"
	if (tokens[index].kind == Token_Sub) {
		
				index++;
	}

	if (IsLiteral(tokens[index].kind) || IsType(tokens, index)) {

		return true;
	}



	// NOTE(Brendan): We have an increment or decrement operator. Now we just need to know if we're valid.
	if (tokens[index].kind == Token_Increment || tokens[index].kind == Token_Decrement) {

		bool hasValue = false;

		u32 tempIndex = index - 1;


		// NOTE(Brendan): Checking the token before our increment/decrement token to see if it's a value.
		if (IsLiteral(tokens[tempIndex].kind) || IsType(tokens, tempIndex)) {

			hasValue = true;
		}


		tempIndex = index + 1;

		// NOTE(Brendan): Now we're going to check the token after our increment/decrement token to see if it's a value.
		if (IsLiteral(tokens[tempIndex].kind) || IsType(tokens, tempIndex)) {


			// NOTE(Brendan): Checking to make sure we don't have something like this:
			// var1 ++ var2;
			if (hasValue) {

				parser_err(tokens[index], "Attempted to use unary operator \"" + tokens[index].text + "\" as a binary operator on tokens  \"" + 
					tokens[index - 1].text + "\" and  \"" + tokens[index + 1].text + "\".");

				return false;
			}

			hasValue = true;
		}


		return hasValue;
	}


	// debug 
	/*
	// NOTE(Brendan): Checking for implicit arrays, like:
	// var1 = {0, 4, -12, var2 * 6};
	if (tokens[index].kind == Token_OpenBrace
		&& (IsLiteral(tokens[index + 1].kind)
			|| IsType(tokens, index))) {

		return true;
	} */


	return false;
}


/* force_inline_all */ bool CreateNewBlock(Token token) {

	if (token.kind != Token_OpenBrace) {

		parser_err(token, "Unexpected token \"" + token.text + "\": Expected token \"{\" when attempting to create a new block.");

		return false;
	}


	//debug
	parser_wrn(token, "Creating a new block!");


	//debug
	if (currentBlock == nullptr) {

		//debug
		parser_wrn(token, "All of my what.");

	}


	//debug
	parser_wrn(token, "parentBlocks size = \"" + std::to_string(parentBlocks.size()) + "\".");


	//debug
	if (!parentBlocks.empty()) {

		//debug
		parser_wrn(token, "parentBlocks.back()->kind = \"" + NODE_TYPE_NAME(parentBlocks.back()) + "\".");
	}


	//debug
	parser_wrn(token, "Still ok! 1");



	parentBlocks.push_back(currentBlock);

	//debug
	parser_wrn(token, "Still ok! 2");

	currentBlock = create_node_block(token, std::vector<AstNode*>());

	//debug
	parser_wrn(token, "Still ok! 3");

	return true;
}


force_inline_all bool CloseCurrentBlock(Token token) {

	if (parentBlocks.empty()) {

		parser_err(token, "Tried to close the top level block! This likely means you have a closing brace without a matching open brace.");

		return false;
	}


	currentBlock = parentBlocks.back();

	parentBlocks.pop_back();

	return true;
}


// NOTE(Brendan): If this is a valid expression, this returns a Binary Expression.
// If there is only a single value, this returns whatever it receives from GetType.
// Otherwise, this returns null.
// The argument "cutoff" is not something you will likely ever have to manually set or bother with.
// "cutoff" is a position in the tokens vector (just like index). When index reaches this value, we stop parsing and return.
// (This is mostly useful for multiple recursive loops through the same expression, like for order of operations or sets of parenthesis.)
AstNode* parse_expression(std::vector<Token> tokens, u32 &index, u32 cutoff) {


	// NOTE(Brendan): In other words, this is our first loop, so we need to figure out how long the expression is.
	if (cutoff == 0) {


		parser_wrn(tokens[index], "\n\n\n\n\n\n\n\n\n\nFirst loop! Token is: " + tokens[index].text);


		// NOTE(Brendan): The loop ends whenever it finds a comma, semicolon, open brace, or conditionally a close parenthesis. 
		// (In other words, it's looking for the end of the expression.)
		for (u32 orderCheck = index; true; orderCheck++) {

			TokenKind kind = tokens[orderCheck].kind;


			//debug
			parser_wrn(tokens[orderCheck], "Current expression token: " + tokens[orderCheck].text);


			// NOTE(Brendan): Just checking for all of the tokens you would expect to find in an expression that we don't need to worry about.
			if (IsValue(tokens, orderCheck) || IsArithmetic(kind) || IsComparison(kind)
				|| kind == Token_Period || kind == Token_Increment || kind == Token_Decrement) {

				continue;
			}


			// NOTE(Brendan): This just tells us when to stop.
			else if (kind == Token_Comma || kind == Token_Semicolon || kind == Token_OpenBrace || kind == Token_CloseBrace) {


				return parse_expression(tokens, index, orderCheck);
			}


			// NOTE(Brendan): When we hit an open parenthesis or bracket, we need to make sure there is a matching closing parenthesis or bracket.
			else if (kind == Token_OpenParen || kind == Token_OpenBracket) {


				// NOTE(Brendan): These two TokenKind declarations are just keeping track of which kind of opening token we've found - either a
				// parenthesis or a bracket. Once we know what sort of opening token we have, then we also set the matching closing token.
				TokenKind open = kind;
				TokenKind closing = (kind == Token_OpenParen ? Token_CloseParen : Token_CloseBracket);


				// NOTE(Brendan): Since we know what token we *do* have, we need to also set the opening and closing tokens we do *not* have so we can ignore them.
				TokenKind ignoreOpen = (kind == Token_OpenParen ? Token_OpenBracket : Token_OpenParen);
				TokenKind ignoreClosing = (kind == Token_OpenParen ? Token_CloseBracket : Token_CloseParen);


				// NOTE(Brendan): We are already on one parenthesis, so we want to start at a depth of one. Loop until we find the matching closing parenthesis.
				for (int depth = 1; depth > 0;) {

					kind = tokens[++orderCheck].kind;



					// NOTE(Brendan): Keep in mind we don't care about these tokens for now - we only want parenthesis.
					if (IsValue(tokens, orderCheck) || IsArithmetic(kind) || IsComparison(kind) || kind == ignoreOpen || kind == ignoreClosing
						|| kind == Token_Period || kind == Token_Increment || kind == Token_Decrement) {

						continue;
					}

					else if (kind == closing) {

						depth--;
					}

					else if (kind == open) {

						depth++;
					}

					else if (kind == Token_EOF) {

						parser_err(tokens[index], "Encountered EOF while parsing " + 
							string(open == Token_OpenParen ? "parenthetical" : "array index") + " expression!");

						// Note(Brendan): This forces the parser to end.
						index = orderCheck;

						return nullptr;
					}

					else {

						parser_err(tokens[orderCheck], "Encountered unexpected token \"" + tokens[orderCheck].text + 
							"\" while attempting to parse a" + string(open == Token_OpenParen ? " parenthetical" : "n array index") + " expression.");

						return nullptr;
					}
				}
			}



			// NOTE(Brendan): In this case, we've encountered a closing parenthesis or bracket without a matching opening parenthesis or bracket.
			// Chances are, we're inside of a function call's argument list or we're an expression determining an array index. For example:
			// my_func(4 + var1);
			// someArray[someVar + 1];
			else if (kind == Token_CloseParen || kind == Token_CloseBracket) {

				return parse_expression(tokens, index, orderCheck);
			}


			// NOTE(Brendan): Always gotta have your EOF check, just in case.
			else if (kind == Token_EOF) {

				parser_err(tokens[index], "Encountered EOF while parsing expression!");

				// Note(Brendan): This forces the parser to end.
				index = orderCheck;

				return nullptr;
			}


			else {

				parser_err(tokens[orderCheck], "Encountered unexpected token \"" + tokens[orderCheck].text + "\" of type \"" + 
					token_names[tokens[orderCheck].kind] + "\" while parsing expression. Unable to continue!");

				return nullptr;
			}
		}
	}


	// NOTE(Brendan): Ultimately, working our way backwards through the expression is the easiest way to
	// evaluate it with respect to the order of operations.
	// This is the second of four loops looking for arithmetic operators; this one looks for comparison operators.
	// (Also, cutoff is going to be the first token _AFTER_ our expression, such as a semicolon, thus we begin at cutoff - 1.)
	for (u32 i = cutoff - 1; i > index; i--) {


		if (IsComparison(tokens[i].kind)) {


			// NOTE(Brendan): Parse the left side of the expression first, starting from index and using 
			// the current comparison operator as our cutoff.
			AstNode* left = parse_expression(tokens, index, i);


			if (left == nullptr) {

				parser_err(tokens[index], "Unable to parse the left side of comparison operator \"" + tokens[i].text + "\".");

				return nullptr;
			}

			// NOTE(Brendan): Now that we've parsed the left side of the comparison operator, 
			// move index past it so we can parse the right.
			index = i + 1;

			// NOTE(Brendan): Now we grab the right side, using our new starting index and original cutoff.
			AstNode* right = parse_expression(tokens, index, cutoff);



			if (right == nullptr) {

				parser_err(tokens[index], "Unable to parse the left side of comparison operator \"" + tokens[i].text + "\".");

				return nullptr;
			}



			//debug
			parser_wrn(tokens[index], "comparison completed. Token: " + tokens[index].text);


			return create_node_binary_expression(tokens[i], left, right);
		}
	}


	// NOTE(Brendan): This is the second of four loops looking for arithmetic operators; this one looks for parenthesis, and is also probably the most complicated.
	// (Also, cutoff is going to be the first token _AFTER_ our expression, such as a semicolon, thus we begin at cutoff - 1.)
	for (u32 i = cutoff - 1; i > index; i--) {


		// NOTE(Brendan): We found a closing parenthesis, now we must find an opening one.
		if (tokens[i].kind == Token_CloseParen) {


			// NOTE(Brendan): We increment this if we encounter another closing parenthesis in our search for an opening one.
			// That way we can make sure we only get matching pairs, not just whichever open parenthesis happens to be first.
			int parenDepth = 0;


			u32 newCutoff = i;


			// NOTE(Brendan): This is important: If we didn't decrement i here, it would still be the first token
			// we checked in the following loop, which would throw off our parenDepth count.
			i--;


			// NOTE(Brendan): Begin looking for the open parenthesis;
			for (; i >= index; i--) {


				if (tokens[i].kind == Token_OpenParen) {


					// NOTE(Brendan): Check to make sure we aren't parenthesis around a function call and proceed as necessary.
					if (!IsValue(tokens, i - 1)) {

						if (parenDepth == 0) {

							AstNode* left = nullptr;
							AstNode* result = nullptr;


							// NOTE(Brendan): If the start of our expression comes before our open parenthesis, then obviously we
							// need to parse that part as well.
							if (i != index) {

								// NOTE(Brendan): i currently points to the open parenthesis. If there's more to the expression
								// before this than the token before this parenthesis must be an arithmetic operator.
								// As such, in order to proceed we need to move i so that it points to this operator.
								i--;


								left = parse_expression(tokens, index, i);

								// NOTE(Brendan): Return i to where it belongs.
								i++;


								if (left == nullptr) {

									parser_err(tokens[i], "Unable to parse the expression to the left of an open parenthesis. "
										"(Did you make sure there's an arithmetic operator between any variables or values and the open parenthesis?)");

									return nullptr;
								}
							}


							// NOTE(Brendan): i.e. we want parsing to begin at the token after the open parenthesis.
							// In other words, our current position (i) + 1
							index = i + 1;


							result = parse_expression(tokens, index, newCutoff);


							if (result == nullptr) {

								parser_err(tokens[i], "Unable to parse a parenthetical expression.");

								return nullptr;
							}


							// NOTE(Brendan): Incrementing index so that it doesn't wind up pointing to the close parenthesis.
							index++;
							

							// NOTE(Brendan): The same as the left side check above, except this time we're checking 
							// to the right of the parenthesis.
							if (index != cutoff) {


								//debug
								parser_wrn(tokens[index], "There's more after our closing parenthesis. Token: " + tokens[index].text);
								parser_wrn(tokens[cutoff], "The cutoff token is: " + tokens[cutoff].text);


								// NOTE(Brendan): This bit of code is necessary since using index in the same line of code where it's
								// passed by reference (in this case, to parse_expression) is undefined behavior, so strange things 
								// can happen. Better just to create a temp token for the arithmetic operator and pass it instead.
								Token op = tokens[index];


								// NOTE(Brendan): If there's more to the expression after the closing parenthesis than the token 
								// after it must be an arithmetic operator. As such, in order to proceed we need to move index 
								// so that it points to the token after this operator so we can parse properly.
								index++;



								AstNode* right = parse_expression(tokens, index, cutoff);


								if (right == nullptr) {

									parser_err(op, "Unable to parse the expression to the right of a close parenthesis. "
										"(Did you make sure there's an arithmetic operator between any variables or values and the close parenthesis?)");

									return nullptr;
								}


								result = create_node_binary_expression(op, result, right);
							}



							if (left != nullptr) {

								result = create_node_binary_expression(tokens[i - 1], left, result);
							}



							return result;
						}


						// NOTE(Brendan): If parenDepth does _NOT_ equal 0.
						else {

							parenDepth--;
						}
					}


					// NOTE(Brendan): If we _ARE_ a function call.
					else {

						// NOTE(Brendan): If we are a function call and parenDepth = 0, then the opening parenthesis that
						// triggered this loop was the beginning of a function call, and we can disregard it.
						if (parenDepth == 0) {

							break;
						}


						else {

							parenDepth--;
						}
					}
				}


				// NOTE(Brendan) We have to make sure we're getting the right set of parenthesis, after all.
				else if (tokens[i].kind == Token_CloseParen) {

					parenDepth++;
				}
			}
		}
	}


	// NOTE(Brendan): This is the third of four loops looking for arithmetic operators; this one looks for addition and subtraction.
	// (Also, cutoff is going to be the first token _AFTER_ our expression, such as a semicolon, thus we begin at cutoff - 1.)
	for (u32 i = cutoff - 1; i > index; i--) {


		if (tokens[i].kind == Token_Add || tokens[i].kind == Token_Sub) {


			// NOTE(Brendan): We have to see if this is supposed to indicate a negative value rather than subtraction.
			// First, we check to see if the next token is a value and the previous is an arithmetic, comparison, or assignment  operator 
			// or an opening parenthesis. If so, then we have a negative value.
			// For example:
			// SomeFunc(-6);
			// index = -index;
			// num > -1;
			// var2 := -var1;
			// expr = (-4 * -7);
			// If this is a negative value (and thus not subtraction), then we continue.
			if (tokens[i].kind == Token_Sub 
				&& IsValue(tokens, i + 1)
				&& (IsArithmetic(tokens[i - 1].kind) 
					|| IsAssignment(tokens[i - 1].kind)
					|| IsComparison(tokens[i - 1].kind)
					|| tokens[i - 1].kind == Token_Comma
					|| tokens[i - 1].kind == Token_OpenParen)) {


				continue;
			}

			// NOTE(Brendan): We're on either a plus or minus sign.
			else {

				// NOTE(Brendan): Parse the left side of the expression first, starting from index and using 
				// the current plus or minus sign as our cutoff.
				AstNode* left = parse_expression(tokens, index, i);


				if (left == nullptr) {

					parser_err(tokens[i], "Unable to parse the expression to the left of arithmetic operator \"" + tokens[i].text + "\"");

					return nullptr;
				}


				// NOTE(Brendan): Now that we've parsed the left side of the plus or minus sign, 
				// move index past it so we can parse the right.
				index = i + 1;

				// NOTE(Brendan): Now we grab the right side, using our new starting index and original cutoff.
				AstNode* right = parse_expression(tokens, index, cutoff);


				if (right == nullptr) {

					parser_err(tokens[i], "Unable to parse the expression to the right of arithmetic operator \"" + tokens[i].text + "\"");

					return nullptr;
				}


				return create_node_binary_expression(tokens[i], left, right);
			}
		}
	}

	// NOTE(Brendan): This is the fourth of four loops looking for arithmetic operators; this one looks for multiplication and division.
	// (Also, cutoff is going to be the first token _AFTER_ our expression, such as a semicolon, thus we begin at cutoff - 1.)
	for (u32 i = cutoff - 1; i > index; i--) {


		if (tokens[i].kind == Token_Mul || tokens[i].kind == Token_Div) {

			// NOTE(Brendan): Parse the left side of the expression first, starting from index and using 
			// the current multiplication or division sign as our cutoff.
			AstNode* left = parse_expression(tokens, index, i);


			if (left == nullptr) {

				parser_err(tokens[i], "Unable to parse the expression to the left of arithmetic operator \"" + tokens[i].text + "\"");

				return nullptr;
			}

			// NOTE(Brendan): Now that we've parsed the left side of the multiplication or division sign, 
			// move index past it so we can parse the right.
			index = i + 1;

			// NOTE(Brendan): Now we grab the right side, using our new starting index and original cutoff.
			AstNode* right = parse_expression(tokens, index, cutoff);


			if (right == nullptr) {

				parser_err(tokens[i], "Unable to parse the expression to the right of arithmetic operator \"" + tokens[i].text + "\"");

				return nullptr;
			}


			return create_node_binary_expression(tokens[i], left, right);
		}
	}



	// NOTE(Brendan): Below this point, we should be looking at just single tokens at a time. (Either values or unary operators.)



	//debug
	parser_wrn(tokens[index], "Doing stuff. Token: " + tokens[index].text);




	// NOTE(Brendan): Getting an arithmetic operator here is unusual, so let's see if this is supposed to indicate a negative value.
	// First, we check to see if the next token is a value and the previous is either an arithmetic, comparison, assignment operator, comma, 
	// or an opening parenthesis. If so, then we have a negative value.
	// For example:
	// SomeFunc(-6);
	// index = -index;
	// num > -1;
	// var2 := -var1;
	// var1, var2 = 7, -6;
	// expr = (-4 * -7);
	// If this is a negative value, then we create a unary expression and continue.
	if (tokens[index].kind == Token_Sub 
		&& IsValue(tokens, index + 1)
		&& (IsArithmetic(tokens[index - 1].kind)
			|| IsAssignment(tokens[index - 1].kind)
			|| IsComparison(tokens[index - 1].kind)
			|| tokens[index - 1].kind == Token_Comma
			|| tokens[index - 1].kind == Token_OpenParen)) {


		// NOTE(Brendan): We need to call GetType on the value, not the negative sign. However, we need to keep the negative sign so we can add it to the
		// unary operator node constructor at the end. Thus, a temp variable is needed to hold it.
		Token neg = tokens[index];


		// NOTE(Brendan): Increment index to point to the value, not the sign.
		index++;

		AstNode* expr = GetType(tokens, index);

		if (expr == nullptr) {

			parser_err(neg, "Unable to parse the expression to the right of the unary negative (\"-\") operator at " + neg.pos.get_text_pos() + ".");

			return nullptr;
		}

		return create_node_unary_expr(neg, expr);
	}


	// NOTE(Brendan): Another unary operator check. This time, we're looking for the not operator (!). Here's an example:
	// someBool = !otherBool;
	// If otherBool is true, someBool is assigned false.
	else if (tokens[index].kind == Token_Not) {


		// NOTE(Brendan): We need to call GetType on the value, not the not operator. However, we need to keep the not operator so we can add it to the
		// unary operator node constructor at the end. Thus, a temp variable is needed to hold it.
		Token not_op = tokens[index];


		// NOTE(Brendan): Increment index to point to the value, not the operator.
		index++;

		AstNode* expr = GetType(tokens, index);

		if (expr == nullptr) {

			parser_err(not_op, "Unable to parse the expression to the right of the unary not (\"!\") operator at " + not_op.pos.get_text_pos() + ".");

			return nullptr;
		}

		return create_node_unary_expr(not_op, expr);
	}


	// NOTE(Brendan): We've exhausted all other options, so at this point we must be a value (either a variable or a literal).
	else if (IsValue(tokens, index)) {

		return GetType(tokens, index);
	}


	parser_err(tokens[index], "Unable to parse expression: encountered unexpected token \"" + tokens[index].text + "\" and cannot proceed.");

	return nullptr;
}