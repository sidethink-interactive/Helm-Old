#define LEXER_IMPLEMENTATION
#include "lexer.hpp"
#include <string.h>
#include <sstream>

#define ca(n) (*(t->cursor + n))

struct Tokenizer {
	u32 line = 1, column = 1;
	string file_name;
	string file_contents;
	const char* file_start;
	const char* cursor;
	File* current_file;
};

bool is_whitespace(char test);
bool is_identifier_char(char test, bool first);

#define lexer_err(...) error_at(t->current_file, t->line, t->column, string(__VA_ARGS__), LEXER_FAIL)
#define lexer_wrn(...) warn_at (t->current_file, t->line, t->column, string(__VA_ARGS__))

void eat_whitespace(Tokenizer* t) {
	while(1) {
		if(is_whitespace(*t->cursor)) {
			t->column++;
			t->cursor++;
			continue;
		}
		return;
	}
}

Token eat_block_comment(Tokenizer* t) {
	
	Token token;
	token.kind = Token_Comment;
	token.pos = {t->current_file, t->line, t->column};
	const char* start = t->cursor;
	u8 depth = 0;
	while(1) {
		char c = *t->cursor;
		if(c=='\n') {
			t->line ++;
			t->column = 1;
			t->cursor ++;
			continue;
		} else if(c=='/' && (ca(1) == '*')) {
			depth ++;
			t->column ++;
			t->cursor ++;
		} else if(c=='*' && (ca(1) == '/')) {
			depth --;
			t->column ++;
			t->cursor ++;
		}
		if(depth == 0) {
			break;
		} else {
			t->column ++;
			t->cursor ++;
		}
	}
	// TODO(zachary): Test this!

	// NOTE(Brendan): This crashes if there's a /* with no matching */.
	// (This includes a situation where you have two /* and only one */ by the end of the file.)
	
	t->column ++;
	t->cursor ++;
	token.text = t->file_contents.substr((size_t)(start - t->file_start), (size_t)(t->cursor - start));
	return token;
}

bool naive_strcmp(const char* a, const char* b, bool identifier = true) {
	if(*b==0) return false;
	for(;*b!=0;b++,a++) {
		if(*b!=*a) return false;
	}
	if(identifier) {
		return !is_identifier_char(*a, false);
	}
	return true;
}

char eat_char(Tokenizer* t) {

	char c = *t->cursor;

	if(c=='\n') {
		t->line++;
		t->column = 0;
		return c;
	}
	#define esc(a, b) if(c=='\\' && ca(1) == a) { \
		/* NOTE(zachary): I'm purposefully advancing twice, \
			 to account for the backslash _and_ the 'n' */ \
		t->column++; \
		t->cursor++; \
		return b; \
	}

	// NOTE(zachary): Here's some instructions for implementing escape sequences.
	//   http://en.cppreference.com/w/cpp/language/escape

	esc('n', '\n');
	esc('r', '\r');
	esc('\\', '\\');
	esc('\'', '\'');
	esc('\"', '\"');
	esc('?', '?');
	esc('a', '\a');
	esc('f', '\f');
	esc('t', '\t');
	esc('v', '\v');

	// TODO(zachary): Add more escape sequences.
	//   I'm only doing octal for now because that's
	//   what the majority of escape codes I encounter are.

	// Octal Sequence
	if(c=='\\' && (ca(1) >= '0' && ca(1) <= '7')) {
		// TODO(zachary): There has to be a better way to do this!
		string num;
		num.push_back(ca(1));
		t->column++;
		t->cursor++;
		if(ca(1) >= '0' && ca(1) <= '7') {
			num.push_back(ca(1));
			t->column++;
			t->cursor++;
			if(ca(1) >= '0' && ca(1) <= '7') {
				num.push_back(ca(1));
				t->column++;
				t->cursor++;
			}
		}
		return std::stoi(num, nullptr, 8);
	}

	if(c=='\\') {
		lexer_wrn("Unrecognized escape code");
	}

	return c;

}

Token eat_string(Tokenizer* t) {

	// TODO(zachary): Make sure parsing escaped characters is a good idea!
	// NOTE(zachary): The strings in this language are multi-line strings.
	string contents = "";

	Token token;
	token.kind = Token_String;
	token.pos = {t->current_file, t->line, t->column};

	while(*t->cursor != 0) {
		t->cursor++;
		t->column++;

		if(*t->cursor=='"') {

			t->column++;
			t->cursor++;
			break;
		}

		contents.push_back(eat_char(t));
	}

	token.text = contents;

	return token;

}

std::vector<Token> tokenize_file(fs::path path) {
	if(!exists(path)) {
		print_red("Nonexistant file: ");
		print("\"" << path.string() << "\"\n");
		exit(2);
	} else {
		return tokenize_file(path, read_file(path));
	}
}

std::vector<string> to_lines(string file) {
	std::vector<string> ret;
	std::stringstream ss(file);
	string tmp;
	while(std::getline(ss, tmp, '\n')) {
		ret.push_back(tmp);
	}
	return ret;
}

std::vector<Token> tokenize_file(fs::path file_path, string file_contents) {

	File* current_file = new File;
	current_file->path = file_path;
	current_file->lines = to_lines(file_contents);

	std::vector<Token> result;

	Tokenizer* t = new Tokenizer;
	t->file_name = display_path(file_path);
	t->file_contents = file_contents;

	t->file_start = t->cursor = file_contents.c_str();

	t->current_file = current_file;

lexer_main_loop:
	while(*t->cursor != 0) {

		eat_whitespace(t);

		char c = *t->cursor;

		if(c=='\n') {

			Token token;
			token.kind = Token_NewLine;
			token.pos = {t->current_file, t->line, t->column};

			t->line++;
			t->column = 1;
			t->cursor++;

			continue;
		}

		if(is_identifier_char(c, 1)) {


			Token token;
			token.kind = Token_Identifier;
			token.pos = {t->current_file, t->line, t->column};

			string identifier_name = "";
			identifier_name.push_back(c);
			t->cursor++;
			t->column++;

			// Grabbing an identifier.
			while(*t->cursor != 0) {
				c = *t->cursor;
				if(is_identifier_char(c, 0)) {
					identifier_name.push_back(c);
					t->cursor++;
					t->column++;
				} else break;
			}

			token.text = identifier_name;

			for(int i = Token__KeywordBegin; i < Token__KeywordEnd; i++) {
				if(naive_strcmp(identifier_name.c_str(), token_names[i])) {
					token.kind = (TokenKind)i;
				}
			}

			result.push_back(token);

			continue;
		}

		if(c == '/' && (ca(1) == '*')) {
			result.push_back(eat_block_comment(t));
			continue;
		}

		if(c == '/' && ca(1) == '/') {
			string comment;
			Token token;
			token.kind = Token_Comment;
			t->cursor+=2;
			token.pos = {t->current_file, t->line, t->column};
			while(*t->cursor != 0 && *t->cursor != '\n') {
				comment.push_back(*t->cursor);
				t->cursor++;
			}
			token.text = comment;
			t->cursor++;
			t->column = 0;
			t->line++;
			result.push_back(token);
			continue;
		}


		if((c >= '0' && c <= '9') || (c == '.' && (ca(1) >= '0' && ca(1) <= '9'))) {

			// NOTE(zachary): We have a numeric literal.
			
			Token token;
			token.kind = Token_Integer; // NOTE(zachary): We'll assume
										//   it's an int until proven otherwise.
			token.pos = {t->current_file, t->line, t->column};

			if(c=='.') token.kind = Token_Float;

			string number_value = "";
			number_value.push_back(c);

			int i=0;

			#define MODE_B10 1
			#define MODE_HEX 2
			#define MODE_BIN 3
			u8 mode = MODE_B10;

			while(*t->cursor != 0) {
				i++;
				t->cursor++;
				t->column++;
				c = *t->cursor;
				// TODO(zachary): Is this bulletproof?
				if(mode == MODE_B10) {
					if(c >= '0' && c <= '9') {

						number_value.push_back(c);
						continue;
					} else if(c == 'b') {
						if(i == 1) {
							if(*(t->cursor - 1) != '0') {
								lexer_err("Can't start a binary literal that doesn't begin with 0 (E.g. 0bXX)");
							}
							mode = MODE_BIN;
							number_value.push_back(c);
							continue;
						} else {
							lexer_err("Can't start a binary literal with non-0bXX syntax; the 'b' is in the wrong place!");
						}
					} else if(c == 'x' || c == 'X') {
						if(i == 1) {
							if(*(t->cursor - 1) != '0') {
								lexer_err("Can't start a hexidecimal literal that doesn't begin with 0 (E.g. 0xFFFF)");
							}
							mode = MODE_HEX;
							number_value.push_back(c);
							continue;
						} else {
							lexer_err("Can't start a binary literal with non-0xFFFF syntax; the 'x' is in the wrong place!");
						}
					}
				} else if(mode == MODE_HEX) {

					lexer_err("Hexidecimal literals are not implemented yet!\nSee " __FILE__ " L" + std::to_string(__LINE__));
					
					// Lowercase the character, to make things easier.
					if(c >= 'A' && c <= 'Z') c -= 'A' - 'a';
					

				} else if(mode == MODE_BIN) {
					lexer_err("Binary literals are not implemented yet!\nSee " __FILE__ " L" + std::to_string(__LINE__));
				}
				if(c == '.') {
					if(mode != MODE_B10) {
						lexer_err("Can't put a decimal point in a " + string(mode==MODE_HEX?"hexidecimal":"binary") + " literal");
					}
					token.kind = Token_Float;
					number_value.push_back(c);
					continue;
				}
				break;
			}

			token.text = number_value;

			result.push_back(token);

			continue;
		}

		if(c=='"') {

			result.push_back(eat_string(t));
			continue;

		}

		if(c=='\'') {

			Token token;
			token.kind = Token_Char;
			token.pos = {t->current_file, t->line, t->column};

			t->cursor++;
			t->column++;
			c = eat_char(t);
			if(ca(1)!='\'') {
				lexer_err("Expected a single qoute to end a char literal");
			}
			t->cursor+=2;
			t->column+=2;
			// NOTE(zachary): We'll want chars to be bytes in the language,
			// so it's better to start storing them as numbers sooner
			// rather than later.
			token.text = std::to_string((int)c);
			result.push_back(token);
		}

		// NOTE(zachary): Now, we map the symbols.
		for(int i = Token__AbsoluteCapture; i < Token__KeywordBegin; i++) {
			if(naive_strcmp(t->cursor, token_names[i], false)) {
				int skip = strlen(token_names[i]);
				Token token;
				token.kind = (TokenKind)i;
				token.text = file_contents.substr(t->cursor - t->file_start, skip);
				token.pos = {t->current_file, t->line, t->column};
				result.push_back(token);
				t->cursor += skip;
				t->column += skip;
				// Using goto as a 'continue' that breaks out of both loops.
				goto lexer_main_loop;
			}
		}

		lexer_err("Failed to parse the token!");
		t->cursor++;

	}

	Token EOF_token;
	EOF_token.kind = Token_EOF;
	EOF_token.text = "";
	EOF_token.pos = {t->current_file, t->line, t->column};

	result.push_back(EOF_token);

	return result;
}

// NOTE(zachary): `first` determines whether numbers are allowed.
force_inline_all bool is_identifier_char(char test, bool first) {
	return ((test >= 'a' && test <= 'z') || (test >= 'A' && test <= 'Z') ||
		   (test == '_') || (first && (test <= '0' && test >= '9')) || (first && test == '#') ||
		   !first) && !(is_whitespace(test) || test == '\'' || test == '\"' || 
		   test == '%' || test == '^' || test == '&' || test == '*' || test == '(' || 
		   test == ')' || test == '[' || test == ']' || test == ',' || test == '.' || 
		   test == '/' || test == '<' || test == '>' || test == '?' || test == '+' || 
		   test == '`' || test == ':' || test == ';' || test == '-' || test == '\n' ||
		   test == '{' || test == '}');
}

force_inline_all bool is_whitespace(char test) {

	return test == ' ' || test == '\t' || test == '\r' || test == 0;
}