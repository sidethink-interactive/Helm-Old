#!/bin/sh

# Here's what this is.
# I opened a standard cmd.exe, and ran "set"
# I copy-pasted the output into a diff program. Any should work,
# but I used this: https://www.diffchecker.com
# Then, I ran my "vcvarsall.bat", and then "set" again
# I pasted the output into the other side of the diff program,
# and gathered the results. I took all of the new lines, and converted them
# into variable exports usable in sh. I also modified PATH, so that it
# is a valid sh path export.

export CommandPromptType="Native"
export DevEnvDir="C:\\Program Files (x86)\\Microsoft Visual Studio\\2017\\Community\\Common7\\IDE\\"
export ExtensionSdkDir="C:\\Program Files (x86)\\Microsoft SDKs\\Windows Kits\\10\\ExtensionSDKs"
export Framework40Version="v4.0"
export FrameworkDir="C:\\Windows\\Microsoft.NET\\Framework64\\"
export FrameworkDIR64="C:\\Windows\\Microsoft.NET\\Framework64"
export FrameworkVersion="v4.0.30319"
export FrameworkVersion64="v4.0.30319"
export INCLUDE="C:\\Program Files (x86)\\Microsoft Visual Studio\\2017\\Community\\VC\\Tools\\MSVC\\14.10.25017\\ATLMFC\\include;C:\\Program Files (x86)\\Microsoft Visual Studio\\2017\\Community\\VC\\Tools\\MSVC\\14.10.25017\\include;C:\\Program Files (x86)\\Windows Kits\\NETFXSDK\\4.6.1\\include\\um;C:\\Program Files (x86)\\Windows Kits\\10\\include\\10.0.14393.0\\ucrt;C:\\Program Files (x86)\\Windows Kits\\10\\include\\10.0.14393.0\\shared;C:\\Program Files (x86)\\Windows Kits\\10\\include\\10.0.14393.0\\um;C:\\Program Files (x86)\\Windows Kits\\10\\include\\10.0.14393.0\\winrt;"
export LIB="C:\\Program Files (x86)\\Microsoft Visual Studio\\2017\\Community\\VC\\Tools\\MSVC\\14.10.25017\\ATLMFC\\lib\\x64;C:\\Program Files (x86)\\Microsoft Visual Studio\\2017\\Community\\VC\\Tools\\MSVC\\14.10.25017\\lib\\x64;C:\\Program Files (x86)\\Windows Kits\\NETFXSDK\\4.6.1\\lib\\um\\x64;C:\\Program Files (x86)\\Windows Kits\\10\\lib\\10.0.14393.0\\ucrt\\x64;C:\\Program Files (x86)\\Windows Kits\\10\\lib\\10.0.14393.0\\um\\x64;"
export LIBPATH="C:\\Program Files (x86)\\Microsoft Visual Studio\\2017\\Community\\VC\\Tools\\MSVC\\14.10.25017\\ATLMFC\\lib\\x64;C:\\Program Files (x86)\\Microsoft Visual Studio\\2017\\Community\\VC\\Tools\\MSVC\\14.10.25017\\lib\\x64;C:\\Program Files (x86)\\Windows Kits\\10\\UnionMetadata;C:\\Program Files (x86)\\Windows Kits\\10\\References;C:\\Windows\\Microsoft.NET\\Framework64\\v4.0.30319;"
export NETFXSDKDir="C:\\Program Files (x86)\\Windows Kits\\NETFXSDK\\4.6.1\\"
export Platform="x64"
export UCRTVersion="10.0.14393.0"
export UniversalCRTSdkDir="C:\\Program Files (x86)\\Windows Kits\\10\\"
export VCIDEInstallDir="C:\\Program Files (x86)\\Microsoft Visual Studio\\2017\\Community\\Common7\\IDE\\VC\\"
export VCINSTALLDIR="C:\\Program Files (x86)\\Microsoft Visual Studio\\2017\\Community\\VC\\"
export VCToolsInstallDir="C:\\Program Files (x86)\\Microsoft Visual Studio\\2017\\Community\\VC\\Tools\\MSVC\\14.10.25017\\"
export VCToolsRedistDir="C:\\Program Files (x86)\\Microsoft Visual Studio\\2017\\Community\\VC\\Redist\\MSVC\\14.10.25017\\"
export VisualStudioVersion="15.0"
export VS140COMNTOOLS="C:\\Program Files (x86)\\Microsoft Visual Studio 14.0\\Common7\\Tools\\"
export VS150COMNTOOLS="C:\\Program Files (x86)\\Microsoft Visual Studio\\2017\\Community\\Common7\\Tools\\"
export VSCMD_ARG_app_plat="Desktop"
export VSCMD_ARG_HOST_ARCH="x64"
export VSCMD_ARG_TGT_ARCH="x64"
export VSCMD_VER="15.0.26228.9"
export VSINSTALLDIR="C:\\Program Files (x86)\\Microsoft Visual Studio\\2017\\Community\\"
export WindowsLibPath="C:\\Program Files (x86)\\Windows Kits\\10\\UnionMetadata;C:\\Program Files (x86)\\Windows Kits\\10\\References"
export WindowsSdkBinPath="C:\\Program Files (x86)\\Windows Kits\\10\\bin\\"
export WindowsSdkDir="C:\\Program Files (x86)\\Windows Kits\\10\\"
export WindowsSDKLibVersion="10.0.14393.0\\"
export WindowsSdkVerBinPath="C:\\Program Files (x86)\\Windows Kits\\10\\bin\\10.0.14393.0\\"
export WindowsSDKVersion="10.0.14393.0\\"
export WindowsSDK_ExecutablePath_x64="C:\\Program Files (x86)\\Microsoft SDKs\\Windows\\v10.0A\\bin\\NETFX 4.6.1 Tools\\x64\\"
export WindowsSDK_ExecutablePath_x86="C:\\Program Files (x86)\\Microsoft SDKs\\Windows\\v10.0A\\bin\\NETFX 4.6.1 Tools\\"
export __DOTNET_ADD_64BIT="1"
export __DOTNET_PREFERRED_BITNESS="64"

export PATH="/c/Program Files (x86)/Microsoft Visual Studio/2017/Community/VC/Tools/MSVC/14.10.25017/bin/HostX64/x64:/c/Program Files (x86)/Microsoft Visual Studio/2017/Community/Common7/IDE/VC/VCPackages:/c/Program Files (x86)/Microsoft SDKs/TypeScript/2.1:/c/Program Files (x86)/Microsoft Visual Studio/2017/Community/Common7/IDE/CommonExtensions/Microsoft/TestWindow:/c/Program Files (x86)/Microsoft Visual Studio/2017/Community/Common7/IDE/CommonExtensions/Microsoft/TeamFoundation/Team Explorer:/c/Program Files (x86)/Microsoft Visual Studio/2017/Community/MSBuild/15.0/bin/Roslyn:/c/Program Files (x86)/Microsoft Visual Studio/2017/Community/Team Tools/Performance Tools:/c/Program Files (x86)/Microsoft Visual Studio/Shared/Common/VSPerfCollectionTools/:/c/Program Files (x86)/Microsoft SDKs/Windows/v10.0A/bin/NETFX 4.6.1 Tools/:/c/Program Files (x86)/Microsoft SDKs/F#/4.1/Framework/v4.0/:/c/Program Files (x86)/Windows Kits/10/bin/x64:/c/Program Files (x86)/Windows Kits/10/bin/10.0.14393.0/x64:/c/Program Files (x86)/Microsoft Visual Studio/2017/Community/MSBuild/15.0/bin:/c/Windows/Microsoft.NET/Framework64/v4.0.30319:/c/Program Files (x86)/Microsoft Visual Studio/2017/Community/Common7/IDE/:/c/Program Files (x86)/Microsoft Visual Studio/2017/Community/Common7/Tools/:$PATH"
alias cl="\"/c/Program Files (x86)/Microsoft Visual Studio/2017/Community/VC/Tools/MSVC/14.10.25017/bin/HostX64/x64/cl.exe\""