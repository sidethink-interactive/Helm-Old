<!-- <img src="logo.png" alt="Helm logo" height="100"> -->

# The Helm Programming Language

Helm is a programming with a loose yet strict syntax that encourages readable, simple code that gets its job done without
overcomplicating the solution for the programmer _or_ the CPU.

## Goals

Helm tries to combine the flexibility and structure of D, the speed and footprint of C, and the simplicity of Javascript.
* Fast and capable for low level programming
* Logical syntax that is fast to parse and compile, while being easily read by a programmer
* Opinionated design focusing on programmers that want a language that can let them work without hand holding
* Abstraction layers and a rich standard library to prevent copy-and-paste madness (easily DRY code)
* String based (mixin) metaprogramming and compile-time function evaluation.
* JIT and native code generation (Code a native game in Helm, and use it as an in-game scripting language)
* Capturing lambdas (MERGE REQUESTS, PLEASE.)
* Fun. This one is really important. I want a programmer to be able to sit down and actually _make_ something.

## Requirements to build and run

###### Subject to change. When `helm` is self-hosting, these will change.

- Windows
	* x86-64
	* MSVC 2015 installed (C99 support)
	* LLVM installed
	* Requires MSVC's link.exe as the linker
		* run `vcvarsall.bat` to setup the path

- MacOS
	* x86-64
	* LLVM explicitly installed (`brew install llvm`)
	* XCode installed (for the linker)

- GNU/Linux
	* x86-64
	* Build tools (ld)
	* LLVM installed
	* Clang installed (temporary - this is calling the linker for now)

## Dependencies

Boost
Boost::Filesystem

## Roadmap

* Lexer
* Parser
* C backend
* Compiles test program
* Add very basic class system (constructors and deconstructors)
* Implement #compile directive to function like #foreign_system_library, but with C source code
* Operator overloading in classes
* Start writing standard library
* Port compiler to Helm
* LLVM IR backend
* Create custom codegen backend (maybe? Do I need the speed this will provide?)
* Implement JIT (for metaprogramming)
* Create nice JIT API
* Compatability with C++ function signatures
* Compatability with ObjC function signatures...?
* (zachary): Write sample game engine
* Multithreaded compiler?
* Code optimizations