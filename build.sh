#!/bin/bash

SOURCES=$(find src -name '*.c' -o -name '*.cpp' | tr '\n' ' ')

mkdir build 2>/dev/null
rm -r build/* 2>/dev/null

if [[ `uname` == "MSYS"* ]]; then
	
	. ./vcvarsall.sh

	MSYS2_ARG_CONV_EXCL="/Fe;/std;/I;/EHsc;/LIBPATH;/link;/Zi" cl /Zi /EHsc /std:c++latest "/Febuild\\helm.exe" boost_filesystem-vc141-mt-1_64.lib boost_system-vc141-mt-1_64.lib /I C:/local/boost_1_64_0 $SOURCES /link /LIBPATH:C:/local/boost_1_64_0/lib64-msvc-14.1/

	cp /c/local/boost_1_64_0/lib64-msvc-14.1/boost_system-vc141-mt-1_64.dll ./build/
	cp /c/local/boost_1_64_0/lib64-msvc-14.1/boost_filesystem-vc141-mt-1_64.dll ./build/
else
	#                                                                             For force_inline
	g++ $SOURCES -o build/helm -Wall -lboost_filesystem -lboost_system -std=c++11 -Wno-attributes -g -D_GLIBCXX_DEBUG -D_GLIBCXX_DEBUG_PEDANTIC
fi